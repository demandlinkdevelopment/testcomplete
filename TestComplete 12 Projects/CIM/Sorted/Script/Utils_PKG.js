﻿// ascOrdesc = true, is ascending to descending
// ascOrdesc = false, is descending to ascending
function CompareDates(first_date, second_date, ascOrdesc)
{
  if(first_date != "N/A")
  {
    // Save the data string splitted.
    var first  = first_date.split("/");
    var month1 = first[0];
    var day1   = first[1];
    var year1  = first[2];
    
    // Convert string to int.
    month1 = aqConvert.StrToInt(month1);
    day1   = aqConvert.StrToInt(day1);
    year1  = aqConvert.StrToInt(year1);
    
    var mydate1 = aqDateTime.SetDateElements(year1, month1, day1);
    
  }
  else
  { 
    // If N/A put the default date to be compare to the other if 
    // there is a date in the next field.
    var mydate1 = aqDateTime.SetDateElements(1970, 1, 1);
  }
  
  if(second_date != "N/A")
  {
    // Save the data string splitted.
    var second = second_date.split("/");
    var month2 = second[0];
    var day2   = second[1];
    var year2  = second[2];
  
    // Convert string to int.
    month2 = aqConvert.StrToInt(month2);
    day2   = aqConvert.StrToInt(day2);
    year2  = aqConvert.StrToInt(year2);
  
    // Create the date.
    var mydate2 = aqDateTime.SetDateElements(year2, month2, day2);
  }
  else
  { 
    // If N/A put the default date to be compare to the other if 
    // there is a date in the next field.
    var mydate2 = aqDateTime.SetDateElements(1970, 1, 1);
  }
  
  var resultComparison = aqDateTime.Compare(mydate1, mydate2);
  // Ascending to descending.
  if(ascOrdesc)
  {
    if(resultComparison >= 0)
    {
      Log.Message(aqString.Format("Date: %s is equal or later than Date: %s", 
      aqConvert.DateTimeToStr(mydate1), aqConvert.DateTimeToStr(mydate2)));
    }
    else
    {
      Log.Warning(aqString.Format("Date: %s is NOT equal NOR later than Date: %s", 
      aqConvert.DateTimeToStr(mydate1), aqConvert.DateTimeToStr(mydate2)));
    }
  }
  else // Descending to ascending.
  {
    if(resultComparison <= 0)
    {
      Log.Message(aqString.Format("Date: %s is equal or earlier than Date: %s", 
      aqConvert.DateTimeToStr(mydate1), aqConvert.DateTimeToStr(mydate2)));
    }
    else
    {
      Log.Warning(aqString.Format("Date: %s is NOT equal NOR earlier than Date: %s", 
      aqConvert.DateTimeToStr(mydate1), aqConvert.DateTimeToStr(mydate2)));
    }
  }
  
}