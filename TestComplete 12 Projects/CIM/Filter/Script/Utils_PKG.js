﻿// numOfFIlterChilds: list element seems to be on the top of the page, give the number of them.
// wrapperOfElementList: it is depending on the filter, it is not TextNode(integer) it is TextNode(string),
//                       that string depends on the filter type.
// fieldCellMap: it is the map of the cells to verify if the content of the cell is correct when the filter
//               is applied, without the "Cell".
// fieldsTable: it is the mapping for the content of the field cell table, used to retrive the number of rows.
// x_cell: it is the index for the column.
function selectedFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, x_cell, wrapperBodyPanel, exception="")
{
  if(exception == "Location" || exception == "Item" || exception == "Product" || exception == "Assortment")
  {
    specialFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, x_cell, wrapperBodyPanel);
  }
  else
  {
    normalFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, x_cell, wrapperBodyPanel);
  }
}
// This function search, selects, applied the filter and verify the content for filter list where the 
// height of the element of the filter is constant and the elements to be verify is only one value,
// this make the filter faster than the other.
function normalFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, x_cell, wrapperBodyPanel)
{
  // First element of the filter list.
  var filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0);
  // Remove the elements on the top that are not TextNode(integer).
  var realNumberOfChilds = numOfFilterChilds - 4;
  var randomElementIndex = 0;
  var maxIndexingValue = 200;
  
  if(filterElement.contentText == "The results could not be loaded.")
  {
    // Nothing to verify, there are no elements in the list to be displayed.
    Log.Warning("There is no element in the list to be loaded.");
    return;
  }
  else
  {
  
    // Limit the indexing if it is too big to reduce the time of searching.
    if(realNumberOfChilds > maxIndexingValue)
    {
      realNumberOfChilds = maxIndexingValue;
    }
    // Generate randomly the index of the child elements according to the number existing.
    randomElementIndex = Math.floor((Math.random() * 1000)) % realNumberOfChilds;
    // Get the random object.
    // Elements of the list are always in this path, except for the wrapper of the elements that changes the
    // name, instead of an index, that why it is as input of this function.
    filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(randomElementIndex);
    var valueOfFilterApplied = filterElement.contentText;
    // If there are more elements in the list that could be displayed.
    if(realNumberOfChilds > 6)
    {
      var heightValueOfElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0).Height;
      // Take the position of the first visible (at the beginning) element on the screen.
      var topVisibleListWindow = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0).ScreenTop;
      // Take the position of the last visible element.
      var bottomVisibleListWindow = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(5).ScreenTop;
      // While the element is not in the visible part, move the list to be able to see it.
      while(!(filterElement.ScreenTop >= topVisibleListWindow && 
      filterElement.ScreenTop <= bottomVisibleListWindow ))
      {
        wrapperOfElementList.scrollTop = wrapperOfElementList.scrollTop + heightValueOfElement;
        //filterElement.Wait();
        Aliases.browser.pageCimDemandlinkCom.Wait();
      }
      // Then click. (Select the filter).
      filterElement.Click();
      Aliases.browser.pageCimDemandlinkCom.Wait();
    }
    else// the number of element to be displayed fits exactly in the visible section of the list.
    {
      // Select the filter.
      filterElement.Click();
      Aliases.browser.pageCimDemandlinkCom.Wait();
    }
    
    Log.Message("Filter Applied : " + valueOfFilterApplied);
    // Verify if the content of the filter applied it is correct in the fields.
    aqUtils.Delay(5000, "Delay for the Page.");
    fieldsTable.Refresh();
    wrapperBodyPanel.Refresh();
    //aqUtils.Delay(1000, "");
    var numOfRows = fieldsTable.RowCount - 1;
    var iterVar;
    var valueOfCurCell;
    var boxFilterSelectedValue;
    
    // Verify that the same selected item is equal
    // to the element in the body box below filter list.
    boxFilterSelectedValue = wrapperBodyPanel.contentText.slice(0, -6); // remove "close"
    aqObject.CompareProperty(valueOfFilterApplied, cmpEqual, boxFilterSelectedValue, true, 2);
    
    // Verify the content of the field.
    // Cell always start on 1.
    // Verify that the filter applied is the same as the content of the column cell.
    for(iterVar = 1; iterVar <= numOfRows; iterVar++)
    {
      valueOfCurCell = fieldCellMap.Cell(iterVar, x_cell).contentText;
      aqObject.CompareProperty(valueOfFilterApplied, cmpEqual, valueOfCurCell, true, 2);
    }
  }
}

// This function search, selects, applied the filter and verify the content for filter list where the 
// height of the element of the filter is NOT constant and the elements to be verify is two values,
// this make the filter faster than the other.
function specialFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, x_cell, wrapperBodyPanel)
{
  // First element of the filter list.
  var filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0);
  var filterElementSelected;
  // Remove the elements on the top that are not TextNode(integer).
  var realNumberOfChilds = numOfFilterChilds - 4;
  var randomElementIndex = 0;
  var maxIndexingValue = 200;
  
  if(filterElement.contentText == "The results could not be loaded.")
  {
    // Nothing to verify, there are no elements in the list to be displayed.
    Log.Warning("There is no element in the list to be loaded.");
    return;
  }
  else
  {
  
    // Limit the indexing if it is too big to reduce the time of searching.
    if(realNumberOfChilds > maxIndexingValue)
    {
      realNumberOfChilds = maxIndexingValue;
    }
    // Generate randomly the index of the child elements according to the number existing.
    randomElementIndex = Math.floor((Math.random() * 1000)) % realNumberOfChilds;
    // Get the random object.
    // Elements of the list are always in this path, except for the wrapper of the elements that changes the
    // name, instead of an index, that why it is as input of this function.
    filterElementSelected = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(randomElementIndex);
    var valueOfFilterApplied = filterElementSelected.contentText;
    // If there are more elements in the list that could be displayed.
    var heightValueOfElement;
    var visibleTopWindowsList = wrapperOfElementList.screenTop;
    var visibleBottomWindows = visibleTopWindowsList + wrapperOfElementList.Height;
    var iterVar = 0;
    if(wrapperOfElementList.scrollHeight !=  wrapperOfElementList.Height)
    {
      while(iterVar < realNumberOfChilds)
      {
        filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(iterVar);
        heightValueOfElement = filterElement.Height;
        if(filterElementSelected.screenTop >= visibleTopWindowsList && 
        filterElementSelected.screenTop < visibleBottomWindows)
        {
          filterElementSelected.Click();
          Aliases.browser.pageCimDemandlinkCom.Wait();
          break;
        }
        else
        {
          wrapperOfElementList.scrollTop = wrapperOfElementList.scrollTop + heightValueOfElement;
          Aliases.browser.pageCimDemandlinkCom.Wait();
        }
        iterVar = iterVar + 1;
      }
    }
    else
    {
      filterElementSelected.Click();
      Aliases.browser.pageCimDemandlinkCom.Wait();
    }
    Log.Message("Filter Applied : " + valueOfFilterApplied);
    // Verify if the content of the filter applied it is correct in the fields.
    aqUtils.Delay(5000, "Delay for the Page.");
    fieldsTable.Refresh();
    wrapperBodyPanel.Refresh();
    //aqUtils.Delay(1000, "");
    var numOfRows = fieldsTable.RowCount - 1;
    var iterVar;
    var valueOfCurCell,valueOfOtherCurCell;
    var valueOfFilterAppliedSplit, firstColumnContent, secondColumnContent;
    var boxFilterSelectedValue;
    
    valueOfFilterAppliedSplit = valueOfFilterApplied.split(" - ");
    firstColumnContent = valueOfFilterAppliedSplit[0];
    secondColumnContent = valueOfFilterAppliedSplit[1];
    
    // Verify that the same selected item is equal
    // to the element in the body box below filter list.
    boxFilterSelectedValue = wrapperBodyPanel.contentText.slice(0, -6); // remove "close"
    aqObject.CompareProperty(valueOfFilterApplied, cmpEqual, boxFilterSelectedValue, true, 2);
    
    // Verify the content of the field.
    // Cell always start on 1.
    // Verify that the filter applied is the same as the content of the column cell.
    for(iterVar = 1; iterVar <= numOfRows; iterVar++)
    {
      valueOfCurCell = fieldCellMap.Cell(iterVar, x_cell).contentText;
      valueOfOtherCurCell = fieldCellMap.Cell(iterVar, x_cell-1).contentText;
      aqObject.CompareProperty(secondColumnContent, cmpEqual, valueOfCurCell, true, 2);
      aqObject.CompareProperty(firstColumnContent, cmpEqual, valueOfOtherCurCell, true, 2);
    }
  }
}

function cycleCountVerification(fieldCellMap, fieldsTable)
{
  var x_cell = 19; // Cycle count is always this cell index (column).

  // Verify the content of the Cycle Count:
  Log.Message("Filter Applied : " + "Cycle Count.");
  // Verify if the content of the filter applied it is correct in the fields.
  aqUtils.Delay(5000, "Delay for the Page.");
  fieldsTable.Refresh();
  var numOfRows = fieldsTable.RowCount - 1;
  var iterVar;
  // For Cycle Count verify:
  // Cycle Count Indicator column
  var valueOfCurCell;
  // and Cycle Count Date column
  var valueOfOtherCurCell;
  
  // Cell always start on 1.
  for(iterVar = 1; iterVar <= numOfRows; iterVar++)
  {
    valueOfCurCell = fieldCellMap.Cell(iterVar, x_cell).contentText; // Cycle Counter Indicator Colum.
    valueOfOtherCurCell = fieldCellMap.Cell(iterVar, x_cell-1).contentText;// Cycle Count Date Column.
    aqObject.CompareProperty("Reported", cmpEqual, valueOfCurCell, true, 2);
    aqObject.CompareProperty("N/A", cmpNotEqual, valueOfOtherCurCell, true, 2);
  }
}

function consecutiveDaysVerification(numOfFilterChilds, wrapperOfElementList, fieldCellMap, fieldsTable, wrapperBodyPanel)
{
  // Consecutive days is always this cell index (column).
  var x_cell = 26;
  // First element of the filter list.
  var filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0);
  // Remove the elements on the top that are not TextNode(integer).
  var realNumberOfChilds = numOfFilterChilds - 4;
  var randomElementIndex = 0;
  var maxIndexingValue = 200;
  
  if(filterElement.contentText == "The results could not be loaded.")
  {
    // Nothing to verify, there are no elements in the list to be displayed.
    Log.Warning("There is no element in the list to be loaded.");
    return;
  }
  else
  {
    // Limit the indexing if it is too big to reduce the time of searching.
    if(realNumberOfChilds > maxIndexingValue)
    {
      realNumberOfChilds = maxIndexingValue;
    }
    // Generate randomly the index of the child elements according to the number existing.
    randomElementIndex = Math.floor((Math.random() * 1000)) % realNumberOfChilds;
    // Get the random object.
    // Elements of the list are always in this path, except for the wrapper of the elements that changes the
    // name, instead of an index, that why it is as input of this function.
    filterElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(randomElementIndex);
    var valueOfFilterApplied = filterElement.contentText;
    // If there are more elements in the list that could be displayed.
    if(realNumberOfChilds > 6)
    {
      // Consecutive days list element rarely going to have more than two height size, so you only need the first.
      var heightValueOfElement = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0).Height;
      // Take the position of the first visible (at the beginning) element on the screen.
      var topVisibleListWindow = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(0).ScreenTop;
      // Take the position of the last visible element.
      var bottomVisibleListWindow = Sys.Browser("*").Page("https://cim.demandlink.com/?UserName=*").TextNode(5).ScreenTop;
      // While the element is not in the visible part, move the list to be able to see it.
      while(!(filterElement.ScreenTop >= topVisibleListWindow && 
      filterElement.ScreenTop <= bottomVisibleListWindow ))
      {
        wrapperOfElementList.scrollTop = wrapperOfElementList.scrollTop + heightValueOfElement;
        //filterElement.Wait();
        Aliases.browser.pageCimDemandlinkCom.Wait();
      }
      // Then click. (Select the filter).
      filterElement.Click();
    }
    else// the number of element to be displayed fits exactly in the visible section of the list.
    {
      // Select the filter.
      filterElement.Click();
    }
  }
  // Verify the content of consecutive days filter:
  Log.Message("Filter Applied : " + valueOfFilterApplied);
  
  // Verify if the content of the filter applied it is correct in the fields.
  aqUtils.Delay(5000, "Delay for the Page.");
  fieldsTable.Refresh();
  wrapperBodyPanel.Refresh();
  var numOfRows = fieldsTable.RowCount - 1;
  // Value of the filter applied in integer:
  var valueOfFilterAppliedInt = aqConvert.StrToInt(valueOfFilterApplied);
  var iterVar;
  // Consecutive days:
  var valueOfCurCell; // String.
  var valueOfCurCellInt; // integer
  var boxFilterSelectedValue;
  
  // Verify that the same selected item is equal
  // to the element in the body box below filter list.
  boxFilterSelectedValue = wrapperBodyPanel.contentText.slice(0, -6); // remove "close"
  aqObject.CompareProperty(valueOfFilterApplied, cmpEqual, boxFilterSelectedValue, true, 2);
  
  // Verify the content of the field.
  // Cell always start on 1.
  for(iterVar = 1; iterVar <= numOfRows; iterVar++)
  {
    valueOfCurCell = fieldCellMap.Cell(iterVar, x_cell).contentText; // Cycle Counter Indicator Colum.
    valueOfCurCellInt = aqConvert.StrToInt(valueOfCurCell);
    // Current selected filter value should be equal or greater than displayed in the column cell.
    aqObject.CompareProperty(valueOfCurCellInt, cmpGreaterOrEqual, valueOfFilterAppliedInt, true, 2);
  }
}