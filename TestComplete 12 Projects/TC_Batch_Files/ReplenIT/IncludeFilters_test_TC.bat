REM Clears the screen
CLS
@ECHO OFF
REM Launches TestComplete,
REM executes the specific project 
REM and closes TestComplete when is over
"C:\Program Files (x86)\SmartBear\TestComplete 12\Bin\TestComplete.exe" "D:\TestComplete\TestComplete 12 Projects\ReplenIT\ReplenIT.pjs" /r /e /SilentMode /project:IncludeFilters /test:"Script|RunIncludeTest|Test1"
IF ERRORLEVEL 1001 GOTO NotEnoughDiskSpace
IF ERRORLEVEL 1000 GOTO AnotherInstance
IF ERRORLEVEL 127 GOTO DamagedInstall
IF ERRORLEVEL 4 GOTO Timeout
IF ERRORLEVEL 3 GOTO CannotRun
IF ERRORLEVEL 2 GOTO Errors
IF ERRORLEVEL 1 GOTO Warnings
IF ERRORLEVEL 0 GOTO Success
IF ERRORLEVEL -1 GOTO LicenseFailed
 
:NotEnoughDiskSpace
ECHO There is not enough free disk space to run TestComplete
GOTO End
 
:AnotherInstance
ECHO Another instance of TestComplete is already running
GOTO End
 
:DamagedInstall
ECHO TestComplete installation is damaged or some files are missing
GOTO End
 
:Timeout
ECHO Timeout elapses
GOTO End
 
:CannotRun
ECHO The script cannot be run
GOTO End
 
:Errors
ECHO There are errors
GOTO End
 
:Warnings
ECHO There are warnings
GOTO End
 
:Success
ECHO No errors
GOTO End
 
:LicenseFailed
ECHO License check failed
GOTO End
 
:End