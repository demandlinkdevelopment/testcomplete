﻿function Test1()
{
  // ***************************************************
  // Function test parameters:
  // 1: Username
  // 2: Password
  // 3: browser selector: chrome, IE, firefox or edge
  // 4: URL selector: ReplenIT(not implemented for this test), ReplenIT_2, ReplenIT_3
  // ***************************************************
  // Run User Name: testreplenityoungs
  // ************* DO NOT RUN FOR REPLENIT (no implemented), JUST FOR ReplenIT2 and ReplenIT3
  //KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "chrome", "ReplenIT");
  //KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "firefox", "ReplenIT");
  
  KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "chrome", "ReplenIT_2");
  KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "firefox", "ReplenIT_2");
  
  KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "chrome", "ReplenIT_3");
  KeywordTests.BulkTest.Run("testreplenityoungs","Replenit123", "firefox", "ReplenIT_3");
  
  // Run User Name: testreplenitacosta
  // KeywordTests.BulkTest.Run("testreplenitacosta","Replenit123", "chrome", "ReplenIT");
  
  // *************************************************************
  // Verify warnings and send e-mail in case there are:
  // *************************************************************
  // Save the current log before to verify that there are warnings.
  Log.SaveToDisk();
  
  // Verify if there is a warning and create the message to be sent:
  // Variable to keep track the information.
  // Variables for log objects:
  var lastDataLogEntry, currentLog, currentLogElement;
  // var currentLogElementValue, currentLogElementType;
  // Variables for strings.
  var messageString = "", currentUserName = "";
  var currentBrowserType = "", currentReplenITURL = "";
  var exceptionBrowserStr = "The browser is already running.";
  // Iteration variable.
  var i = 0;
  // Flags and counters.
  var exceptionBrowserFlag = false, warningsCounter = 0;
  //var scheme;
  
  if(Project.Logs.LogItemsCount > 0 )
  {
    // Retrieve the last log.
    lastDataLogEntry = Project.Logs.LogItem(Project.Logs.LogItemsCount - 1);
    
    if(lastDataLogEntry.Status == 1)// There is a warning in the log.
    {
      // All the log data seems to be in this index.
      currentLog = lastDataLogEntry.Data(0);
      // Scheme is always a table tree scheme.
      // scheme = lastDataLog.Data(0).Scheme.DataType;
      
      for(i = 0; i < currentLog.RowCount; i++)
      {
        // take the row of the current log entry.
        currentLogElement = currentLog.Rows(i);
        //currentLogElementValue = currentLogElement.ValueByName("Message");
        //currentLogElementType = currentLogElement.ValueByName("Type Description");
        
        // Current browser type:
        if(currentLogElement.ValueByName("Message").search("Test Executed in Browser") != -1)
        {
          // If a new browser is tested, but there is warning in the last previous,
          // column, create the email message for the previous warnings.
          if(warningsCounter >= 1)
          {
            messageString = messageString
            + "In Browser: \"" + currentBrowserType + "\", \n"
            + "In Replen IT URL:\"" + currentReplenITURL + "\", \n"
            + "The User Name: \"" + currentUserName + "\" has a BAD behavior \""
            + "when testing the summation and subtraction values in Edited Replen Sugg. \n";
            warningsCounter = 0; // Restart counter.
          }
          // Save the new user name after creating the message with the previous info.
          currentBrowserType = currentLogElement.ValueByName("Message").split(":");
          currentBrowserType = currentBrowserType[1];
        }
        
        // Current URL address:
        if(currentLogElement.ValueByName("Message").search("Test Executed in Replen URL") != -1)
        {
          // If a new browser is tested, but there is warning in the last previous,
          // column, create the email message for the previous warnings.
          if(warningsCounter >= 1)
          {
            messageString = messageString
            + "In Browser: \"" + currentBrowserType + "\", \n"
            + "In Replen IT URL:\"" + currentReplenITURL + "\", \n"
            + "The User Name: \"" + currentUserName + "\" has a BAD behavior \""
            + "when testing the summation and subtraction values in Edited Replen Sugg. \n";
            warningsCounter = 0; // Restart counter.
          }
          // Save the new user name after creating the message with the previous info.
          currentReplenITURL = currentLogElement.ValueByName("Message").split("::");
          currentReplenITURL = currentReplenITURL[1];
        }
        
        // Current user name:
        if(currentLogElement.ValueByName("Message").search("UserName") != -1)
        {
          // If a new user name is tested, but there is warning in the last previous,
          // column, create the email message for the previous warnings.
          if(warningsCounter >= 1)
          {
            messageString = messageString
            + "In Browser: \"" + currentBrowserType + "\", \n"
            + "In Replen IT URL:\"" + currentReplenITURL + "\", \n"
            + "The User Name: \"" + currentUserName + "\" has a BAD behavior \""
            + "when testing the summation and subtraction values in Edited Replen Sugg. \n";
            warningsCounter = 0; // Restart counter.
          }
          // Save the new user name after creating the message with the previous info.
          currentUserName = currentLogElement.ValueByName("Message").split(":");
          currentUserName = currentUserName[1];
        }
        
        // If it is the last element in the log entry create the message if 
        // there was warnings for the elements:
        if(i == currentLog.RowCount - 1 && warningsCounter >= 1)
        {
            messageString = messageString
            + "In Browser: \"" + currentBrowserType + "\", \n"
            + "In Replen IT URL:\"" + currentReplenITURL + "\", \n"
            + "The User Name: \"" + currentUserName + "\" has a BAD behavior \""
            + "when testing the summation and subtraction values in Edited Replen Sugg. \n";
            warningsCounter = 0; // Restart counter.
        }
        
        if(currentLogElement.ValueByName("Type Description") == "Warning")// warning in the current data.
        {          
          // See if the warning is because the browser was running.
          if(currentLogElement.ValueByName("Message") == exceptionBrowserStr)
          {
            exceptionBrowserFlag = true; // Set the flag.
          }
          else
          {
            warningsCounter = warningsCounter + 1;
          }
        }
      }
    }
    else if(lastDataLogEntry.Status == 2)// There is an error. This is handle by below handler. 
    {
      messageString = "Some unexpected error has occurred in the log: \n" 
      + lastDataLog.Name;
    }
  } 
  
  // Send e-mail to support.
  var toAddress = "support@demandlink.com";
  //var toAddress = "atorres@demandlink.com";
  //var fromHost = "mail.demandlink.com";
  //var fromName = "TestComplete";
  var fromAddress = "operations@demandlink.com";
  var emailSubject = "TestComplete Edited Replen Sugg test for ReplenIT.";
  var emailBody = "";
  
  emailBody = messageString;

  if(emailBody != "")// If message is not empty that mean that there were warnings.
  {
    SendEmail(fromAddress, toAddress, emailSubject, emailBody);
    //Log.Message(emailBody);
  }
}

// When an Error occurred send a message,
// Seems that only is called when an error occurred
// but not when the execution is stopped.
function GeneralEvents_OnLogError(Sender, LogParams)
{
  //Log.Message("OnLogError!!!! - Edited Replen Sugg");
  
  // Send e-mail to support.;
  var toAddress = "support@demandlink.com";
  //var toAddress = "atorres@demandlink.com";
  var fromAddress = "operations@demandlink.com";
  var emailSubject = "ERROR OCURRED in The TestComplete Edited Replen Sugg test for ReplenIT.";
  var emailBody = "An unexpected error has ocurred in the Edited Replen Sugg Test for ReplenIT, please review" +
  " the log in TestComplete ReplenIT project for Additional Information.";
  
  SendEmail(fromAddress, toAddress, emailSubject, emailBody);
  
  Runner.Stop(false);
}

function SendEmail(mFrom, mTo, mSubject, mBody)
{
  var schema, mConfig, mMessage;

  try
  {
    schema = "http://schemas.microsoft.com/cdo/configuration/";
    mConfig = getActiveXObject("CDO.Configuration");
    mConfig.Fields.$set("Item", schema + "sendusing", 2); // cdoSendUsingPort
    //mConfig.Fields.$set("Item", schema + "smtpserver", "ServerName"); // SMTP server
    //mConfig.Fields.$set("Item", schema + "smtpserverport", 25); // Port number

    // If you use Gmail --
    // mConfig.Fields.$set("Item", schema + "smtpserver", "smtp.gmail.com");
    // mConfig.Fields.$set("Item", schema + "smtpserverport", 25);

    // If you use Outlook --
    mConfig.Fields.$set("Item", schema + "smtpserver", "192.168.10.14");
    mConfig.Fields.$set("Item", schema + "smtpserverport", 25);

    // If you use Office365 --
    // mConfig.Fields.$set("Item", schema + "smtpserver", "smtp.office365.com");
    // mConfig.Fields.$set("Item", schema + "smtpserverport", 587);

    mConfig.Fields.$set("Item", schema + "smtpauthenticate", 1); // Authentication mechanism
    // mConfig.Fields.$set("Item", schema + "sendusername", ""); // User name (if needed)
    // mConfig.Fields.$set("Item", schema + "sendpassword", ""); // User password (if needed)
    mConfig.Fields.Update();

    mMessage = getActiveXObject("CDO.Message");
    mMessage.Configuration = mConfig;
    mMessage.From = mFrom;
    mMessage.To = mTo;
    mMessage.Subject = mSubject;
    mMessage.HTMLBody = mBody;

    //aqString.ListSeparator = ",";
    //for(let i = 0; i < aqString.GetListLength(mAttach); i++)
    //  mMessage.AddAttachment(aqString.GetListItem(mAttach, i));
    mMessage.Send();
  }
  catch (exception)
  {
    Log.Error("Email cannot be sent", exception.message);
    return false;
  }
  Log.Message("Message to <" + mTo + "> was successfully sent");
  return true;
}