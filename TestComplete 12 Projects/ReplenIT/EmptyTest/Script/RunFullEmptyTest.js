﻿// LEGACY TEST
function Test1()
{
  
  // Run User Name: MorrisMiller
  KeywordTests.EmptyFullTestCrhm.Run("MorrisMiller","tinclad75&");

  // Run User Name: CrystalFoley
  KeywordTests.EmptyFullTestCrhm.Run("CrystalFoley","kerslam20!");
  
  // Run User Name: ChrisHallene
  KeywordTests.EmptyFullTestCrhm.Run("ChrisHallene","Amores10%");
  
  // Run User Name: DeoMartinez
  KeywordTests.EmptyFullTestCrhm.Run("DeoMartinez","72W3Og%!n1");
  
  // Run User Name: JaneMansonCS
  KeywordTests.EmptyFullTestCrhm.Run("JaneMansonCS","nubble50#");
  
  // Run User Name: ConnieLankford
  KeywordTests.EmptyFullTestCrhm.Run("ConnieLankford","cosmic30@");
  
  // Run User Name: NatalieBrooksLT
   KeywordTests.EmptyFullTestCrhm.Run("NatalieBrooksLT","staiver26$");
  
  // Run User Name: AaronVanBaal
  KeywordTests.EmptyFullTestCrhm.Run("AaronVanBaal","contort78$");
  
  // Run User Name: BrianBuss
  KeywordTests.EmptyFullTestCrhm.Run("BrianBuss","adjustment9L!");
  
  // Run User Name: DaveHuffaker
  KeywordTests.EmptyFullTestCrhm.Run("DaveHuffaker","alcanna38#");
  
  // Run User Name: MeganMyersLow
  KeywordTests.EmptyFullTestCrhm.Run("MeganMyersLow","Loup43!");
  
  // Run User Name: MeganBankston
  KeywordTests.EmptyFullTestCrhm.Run("MeganBankston","mastery22#");
  
  // Run User Name: LynetteMichon
  KeywordTests.EmptyFullTestCrhm.Run("LynetteMichon","dllm2013^^*");
  
  // Run User Name: ChrisMontgomery
  KeywordTests.EmptyFullTestCrhm.Run("ChrisMontgomery","dlcm2013#^$");
  
  // Run User Name: KaitlynnTaylor
  KeywordTests.EmptyFullTestCrhm.Run("KaitlynnTaylor","warmly12%");
  
  // *************************************************************
  // Verify warnings and send e-mail in case there are:
  // *************************************************************
  // Save the current log before to verify that there are warnings.
  Log.SaveToDisk();
  
  // Verify if there is a warning and create the message to be sent:
  // Variable to keep track the information.
  // Variables for log objects:
  var lastDataLogEntry, currentLog, currentLogElement;
  // var currentLogElementValue, currentLogElementType;
  // Variables for strings.
  var messageString = "", currentColumnName = "", currentUserName = "";
  var exceptionBrowserStr = "The browser is already running.";
  // Iteration variable.
  var i = 0;
  // Flags and counters.
  var exceptionBrowserFlag = false, warningsCounter = 0;
  //var scheme;
  
  if(Project.Logs.LogItemsCount > 0 )
  {
    // Retrieve the last log.
    lastDataLogEntry = Project.Logs.LogItem(Project.Logs.LogItemsCount - 1);
    
    if(lastDataLogEntry.Status == 1)// There is a warning in the log.
    {
      // All the log data seems to be in this index.
      currentLog = lastDataLogEntry.Data(0);
      // Scheme is always a table tree scheme.
      // scheme = lastDataLog.Data(0).Scheme.DataType;
      
      for(i = 0; i < currentLog.RowCount; i++)
      {
        // take the row of the current log entry.
        currentLogElement = currentLog.Rows(i);
        //currentLogElementValue = currentLogElement.ValueByName("Message");
        //currentLogElementType = currentLogElement.ValueByName("Type Description");
        
        // Current user name:
        if(currentLogElement.ValueByName("Message").search("UserName") != -1)
        {
          // If a new user name is tested, but there is warning in the last previous,
          // column, create the email message for the previous warnings.
          if(warningsCounter >= 1)
          {
            messageString = messageString
            + "The User Name: " + currentUserName + " in the column \""
            + currentColumnName + "\" has emptiness in fields. \n";
            warningsCounter = 0; // Restart counter.
          }
          // Save the new user name after creating the message with the previous info.
          currentUserName = currentLogElement.ValueByName("Message").split(":");
          currentUserName = currentUserName[1];
        }
        
        // Current Column:
        if(currentLogElement.ValueByName("Message").search("Testing emptiness column") == 0)
        {
          // If a new column name is tested, but there is warning in the last previous,
          // column, create the email message for the previous warnings.
          if(warningsCounter >= 1)
          {
            messageString = messageString
            + "The User Name: " + currentUserName + " in the column \""
            + currentColumnName + "\" has emptiness in fields.\n";
            warningsCounter = 0; // Restart counter.
          }
          // Save the new column after creating the massage with the previous info.
          currentColumnName = currentLogElement.ValueByName("Message").split(":");
          currentColumnName = currentColumnName[1];
        }
        // If it is the last element in the log entry create the message if 
        // there was warnings for the elements:
        if(i == currentLog.RowCount - 1 && warningsCounter >= 1)
        {
            messageString = messageString
            + "The User Name: " + currentUserName + " in the column \""
            + currentColumnName + "\" has emptiness in fields.\n";
            warningsCounter = 0; // Restart counter.
        }
        
        if(currentLogElement.ValueByName("Type Description") == "Warning")// warning in the current data.
        {          
          // See if the warning is because the browser was running.
          if(currentLogElement.ValueByName("Message") == exceptionBrowserStr)
          {
            exceptionBrowserFlag = true; // Set the flag.
          }
          else
          {
            warningsCounter = warningsCounter + 1;
          }
        }
      }
    }
    else if(lastDataLogEntry.Status == 2)// There is an error.
    {
      messageString = "Some unexpected error has occurred in the log: \n" 
      + lastDataLog.Name;
    }
  } 
  
  // Send e-mail to support.
  var toAddress = "support@demandlink.com";
  //var fromHost = "mail.demandlink.com";
  //var fromName = "TestComplete";
  var fromAddress = "operations@demandlink.com";
  var emailSubject = "TestComplete Emptiness test.";
  var emailBody = "";
  
  emailBody = messageString;

  if(emailBody != "")// If message is not empty that mean that there were warnings.
  {
    SendEmail(fromAddress, toAddress, emailSubject, emailBody);
  }
  
}

function SendEmail(mFrom, mTo, mSubject, mBody)
{
  var schema, mConfig, mMessage;

  try
  {
    schema = "http://schemas.microsoft.com/cdo/configuration/";
    mConfig = getActiveXObject("CDO.Configuration");
    mConfig.Fields.$set("Item", schema + "sendusing", 2); // cdoSendUsingPort
    //mConfig.Fields.$set("Item", schema + "smtpserver", "ServerName"); // SMTP server
    //mConfig.Fields.$set("Item", schema + "smtpserverport", 25); // Port number

    // If you use Gmail --
    // mConfig.Fields.$set("Item", schema + "smtpserver", "smtp.gmail.com");
    // mConfig.Fields.$set("Item", schema + "smtpserverport", 25);

    // If you use Outlook --
    mConfig.Fields.$set("Item", schema + "smtpserver", "192.168.10.14");
    mConfig.Fields.$set("Item", schema + "smtpserverport", 25);

    // If you use Office365 --
    // mConfig.Fields.$set("Item", schema + "smtpserver", "smtp.office365.com");
    // mConfig.Fields.$set("Item", schema + "smtpserverport", 587);

    mConfig.Fields.$set("Item", schema + "smtpauthenticate", 1); // Authentication mechanism
    // mConfig.Fields.$set("Item", schema + "sendusername", ""); // User name (if needed)
    // mConfig.Fields.$set("Item", schema + "sendpassword", ""); // User password (if needed)
    mConfig.Fields.Update();

    mMessage = getActiveXObject("CDO.Message");
    mMessage.Configuration = mConfig;
    mMessage.From = mFrom;
    mMessage.To = mTo;
    mMessage.Subject = mSubject;
    mMessage.HTMLBody = mBody;

    //aqString.ListSeparator = ",";
    //for(let i = 0; i < aqString.GetListLength(mAttach); i++)
    //  mMessage.AddAttachment(aqString.GetListItem(mAttach, i));
    mMessage.Send();
  }
  catch (exception)
  {
    Log.Error("Email cannot be sent", exception.message);
    return false;
  }
  Log.Message("Message to <" + mTo + "> was successfully sent");
  return true;
}