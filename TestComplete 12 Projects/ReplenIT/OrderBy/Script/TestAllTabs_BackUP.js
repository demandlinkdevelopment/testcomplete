﻿function Test1()
{
  var page;
  var form;
  var textbox;
  var cellsPanel;
  // Login information variables.
  var addr_ReplenIT = "https://replen3.demandlink.com/Home/Login";
  var userName = "testreplenityoungs";
  var password = "Replenit123";
  var howManyRows = 10; // options 10, 50, 100, 500, 1000 and 2000.
  var defaultOrAllColumns = true; // true: default columns, false: all columns.
  // Selector of the top tabs: "Edit", "Stores", "Subregion", "Items", "FarmID" and "Exceptions".
  var topTabsSelectoStr = "Edit"; 
  
  // Variables to keep track of the values.
  var numColumns, tableLen;
  var arrayPropName, arrayPropValue;
  var topRowCellsTable;
  var listOfColumns = [], individualColumn;
  var rowNumber, columnOffset, rowOffset;
  var isAscendingChecked = false;
  var isDescendingChecked = false;
  var cellOfTheTable, compareCellObjectResults;
  var firstSampleStr, secondSampleStr;
  var firstSampleResult, secondSampleResult;
  var topTabObject;
  
  // Start Test:
  // - Open Browser in the webpage
  // - Login and wait.
  Browsers.Item(btChrome).Navigate(addr_ReplenIT);
  page = Aliases.browser.pageReplen3DemandlinkComHomeLogo;
  form = page.form;
  textbox = form.textboxUsername;
  textbox.SetText(userName);
  textbox.Keys("[Tab]");
  form.passwordboxPassword.SetText(password);
  textbox.Keys("[Tab]");
  form.submitbuttonLogin.Keys("[Enter]");
  page.Wait();
  
  // Select the panel, cell tables accoring the arrange of the top tab.
  if(topTabsSelectoStr == "Edit")
    cellsPanel = page.panelEdittableWrapper.panel.panel;
  else if(topTabsSelectoStr == "Stores")
    cellsPanel = page.panelStoretableWrapper.panel;
  else if(topTabsSelectoStr == "Subregion")
    cellsPanel = page.panelSubregtableWrapper.panel;
  else if(topTabsSelectoStr == "Items")
    cellsPanel = page.panelItemtableWrapper.panel;
  else if(topTabsSelectoStr == "FarmID")
    cellsPanel = page.panelFarmtableWrapper.panel;
  else if(topTabsSelectoStr == "Exceptions")
    cellsPanel = page.panelExceptiontableWrapper.panel;
  page.Wait();
  
  // Tabs of the top of the web page.
  panelTopTabs = page.panelWrapper.panel.panel.panel;
  // Selects the top tab of the webpage.
  topTabObject = panelTopTabs.FindChild(new Array("contentText"), new Array(topTabsSelectoStr));
  page.Wait();
  if(topTabObject != null)
    topTabObject.Click();
    page.Wait();

  // Selects how many rows.
  page.panelMain.Click(65, 142);
  page.Wait();
  if(howManyRows == 10)
  {
    page.panelRows.Click(74, 18);
  }
  else if(howManyRows == 50)
  {
    page.panelRows.Click(74, 55);
  }
  else if(howManyRows == 100)
  {
    page.panelRows.Click(74, 92);
  }
  else if(howManyRows == 500)
  {
    page.panelRows.Click(74, 120);
  }
  else if(howManyRows == 1000)
  {
    page.panelRows.Click(74, 150);
  }
  else if(howManyRows == 2000)
  {
    page.panelRows.Click(74, 190);
  }
  else // 10 rows
  {
    page.panelRows.Click(74, 18);
  }
  page.Wait();
  
  // Select default or all columns to be verified.
  // Only when the current tab is "Edit".
  if(topTabsSelectoStr == "Edit")
  {
    page.panelMain.Click(193, 148);
    page.Wait();
    if(defaultOrAllColumns)
    {
      page.panelRows.Click(72, 76);
    }
    else
    {
      page.panelRows.Click(72, 42);
    }
    page.Wait();
    // Go out of the column menu by clicking outside.
    page.panelMain.Click(207, 94);
    page.Wait();
  }
  
  // Number of the columns of the top of the table.
  // less 2 due the first column and because is zero based counting.
  // Consider the checkbox column when the current tab is Edit.
  if(topTabsSelectoStr == "Edit")
  {
    columnOffset = 2;
    rowOffset = 2;
  }
  else
  {
    columnOffset = 1;
    rowOffset = 1;
  }
  numColumns = (cellsPanel.table.ChildCount / cellsPanel.table.RowCount)-columnOffset;
  // Rows of the cell table (Seems to have two or one hidden elements):
  tableLen = cellsPanel.tableAllCellObjects.RowCount - rowOffset;

  // Go out if there is no element in the table.
  if(tableLen <= 1 || numColumns <= 0)
  {
    Log.Message("No Elements to compare in the table.");
    return;
  }
   
  // Start the panel screen from the beginning in case is not.
  cellsPanel.panel.scrollLeft = 0;
  cellsPanel.panel.scrollTop = 0;
  
  // Save all the columns in the array
  topRowCellsTable = cellsPanel.table;
  arrayPropName = new Array("ColumnIndex", "RowIndex");
  rowNumber = 0; // Row for the title of each column
  for(i = 0; i <= numColumns; i++)
  {
    arrayPropValue = new Array(i+(columnOffset-1), rowNumber);
    individualColumn = topRowCellsTable.FindChild(arrayPropName, arrayPropValue);
    listOfColumns.push(individualColumn);
  }
  // Edit tab: cells information starts in the 2 row.
  if(topTabsSelectoStr == "Edit")
    rowNumber = 2;
  else
    rowNumber = 1;
  // Edit tab has a offset of 1 in the cellm due the checkbox column.
  // columnOffset = 1;
  // Pointer to all table of the cells.
  allCellsTableEditable = cellsPanel.tableAllCellObjects;
  //var columnCounter = 10;
  for(columnCounter = 0; columnCounter <= numColumns; columnCounter++)
  {
    cellsPanel.panel.scrollTop = 0;
    // Restart tracking variables.
    isAscendingChecked = false;
    isDescendingChecked = false;
    // Retrive the Object Column of the top table List.
    //individualColumn = listOfColumns[columnCounter];
    individualColumn = listOfColumns.shift();
    // Click the column to be sorted the first time.
    individualColumn.Click();
    page.Wait();
    //page.WaitChild(cellsPanel.FullName);
    // Set the horizontal screen.
    setScreenHorizontalScrollWindow(cellsPanel, columnCounter, numColumns);
    page.WaitChild(cellsPanel.FullName);
    
    // Print the current column to ve verified
    Log.message("Testing \"" + individualColumn.contentText + "\" Column.");
    // Search for the first cell value of the column.
    arrayPropValue = new Array(columnCounter+(columnOffset-1), rowNumber);
    cellOfTheTable = allCellsTableEditable.FindChild(arrayPropName, arrayPropValue);
    //cellOfTheTable.Wait();
    //page.WaitChild(allCellsTableEditable.FullName);
    //page.WaitChild(cellsPanel.FullName);
    page.Wait();
    
    // Do it ascending to descending and descending to ascending or backwards.
    do{
      // First sample of the value to identify in which status is: ascending or descending.
      firstSampleStr = cellOfTheTable.contentText;
      // Click the column to be sorted the next time and see which is the current status.
      individualColumn.Click();
      //page.WaitChild(allCellsTableEditable.FullName);
      //cellOfTheTable.Wait();
      page.Wait();
      //page.WaitChild(cellsPanel.FullName);
      // second sample of the value to identify in which status is: ascending or descending depending on first sample.
      secondSampleStr = cellOfTheTable.contentText;

      // Request if the object is greater, less or equal than depending of the type of string
      // that could be a Date, String or number.
      compareCellObjectResults = compareCellObjects(firstSampleStr, secondSampleStr, individualColumn.contentText);

      // Check if the status is from descending to ascending and if it was not checked before.
      if((compareCellObjectResults == 0) && (!isDescendingChecked))
      {
        Log.Message("Testing \"" + individualColumn.contentText + "\" Column for Descending to Ascending.");
        verifyAscOrDesAllObject(allCellsTableEditable, rowNumber, tableLen, true, arrayPropName, columnCounter+(columnOffset-1), cellsPanel, individualColumn.contentText);
        isDescendingChecked = true; // Descending is checked.
      }
      // Check if the status is from ascending to descending and if it was not checked before.
      else if((compareCellObjectResults == 1) && (!isAscendingChecked))
      {
        Log.Message("Testing \"" + individualColumn.contentText + "\" Column for Ascending to Descending.");
        verifyAscOrDesAllObject(allCellsTableEditable, rowNumber, tableLen, false, arrayPropName, columnCounter+(columnOffset-1), cellsPanel, individualColumn.contentText);
        isAscendingChecked = true;
      }
      else
      {
         // Equals - all the elements are the same.
         Log.checkpoint("All the Elements of the \"" + individualColumn.contentText + "\" are equal.");
         isAscendingChecked = true;
         isDescendingChecked = true;
      }
    }while(!isDescendingChecked || !isAscendingChecked);
  
  }
  
  // Go back to default of displaying 10 rows.
  page.panelMain.Click(65, 142);
  page.Wait();
  page.panelRows.Click(74, 18);
  page.Wait();
  // Go to default columns once test finishs.
  // Only when the current tab is "Edit".
  if(topTabsSelectoStr == "Edit")
  {
    page.panelMain.Click(193, 148);
    page.Wait();
    page.panelRows.Click(72, 76);
    page.Wait();
    page.panelMain.Click(207, 94);
    page.Wait();
  }
  // End of Test.
  Log.Message("Test of OrderBy completed.")
}

// This function compares if the object is greater, less or equal depending on the string colected from the cell
// Return:
//  0 - the first element is greater than the second
//  1 - the first element is less than the second.
//  2 - both are equal.
function compareCellObjects(firstCellObjectStr, secondCellObjectStr, exception)
{
  // Verify which one is, string, number, date.
  var resultOfComparing1st = findStringPattern(firstCellObjectStr);
  var resultOfComparing2nd = findStringPattern(secondCellObjectStr);
  // Create variables to track and compare.
  var firstCellObjectInt, secondCellObjectInt;
  var firstCellDateArray, secondCellDateArray;
  var firstCellObjectDate, secondCellObjectDate;
  
  // Compare Dates.
  if((resultOfComparing1st == 4 && resultOfComparing2nd == 4) || exception == "Last Ship")
  {
    // Create a Date Object
    firstCellObjectDate = new Date();
    // If the string is empty, create a Date object to equal to 0.
    if(firstCellObjectStr == "")
    {
      firstCellObjectDate.setFullYear(0,0,0);
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    else
    {
      firstCellDateArray = splitStringBasePattern(firstCellObjectStr);
      firstCellObjectDate.setFullYear(parseInt(firstCellDateArray[2]),parseInt(firstCellDateArray[0])-1, parseInt(firstCellDateArray[1]));
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    
    // Create a Date Object.
    secondCellObjectDate = new Date();
    // If the string is empty, create a Date object to equal to 0.
    if(secondCellObjectStr == "")
    {
      secondCellObjectDate.setFullYear(0,0,0);
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    else
    {
      secondCellDateArray = splitStringBasePattern(secondCellObjectStr);
      secondCellObjectDate.setFullYear(parseInt(secondCellDateArray[2]),parseInt(secondCellDateArray[0])-1, parseInt(secondCellDateArray[1]));
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    // Compare the Date objects:
    if(firstCellObjectDate.getTime() > secondCellObjectDate.getTime())
      return 0; // it is greater.
    else if(firstCellObjectDate.getTime() < secondCellObjectDate.getTime())
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare Strings.
  // Exception must be considered when the string differs when there is no info.
  else if((resultOfComparing1st == 3 && resultOfComparing2nd == 3) || exception == "Farm ID" || exception == "Zone")
  {
    // No need to transform, work directly with strings.
    // Compare:
    if(firstCellObjectStr > secondCellObjectStr)
      return 0; // it is greater.
    else if(firstCellObjectStr < secondCellObjectStr)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare numbers.
  else if(resultOfComparing1st <= 2 && resultOfComparing2nd <= 2)
  {
    firstCellObjectInt = parseFloat(splitStringBasePattern(firstCellObjectStr));
    secondCellObjectInt = parseFloat(splitStringBasePattern(secondCellObjectStr));
    // Compare:
    if(firstCellObjectInt > secondCellObjectInt)
      return 0; // it is greater.
    else if(firstCellObjectInt < secondCellObjectInt)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  else
    Log.Error("Elements in the Cells are not the same type or has the exception.")
}

// Function to verify if it list is ascending or descending by "only" numbers, 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
// exception : name of the column exception where there is no info differs from the other data where it is info in the cell.
function verifyAscOrDesAllObject(tableRef, startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel, exception)
{
  var arraySearch;
  var cellRef, nextCellRef;
  var compareResult;
  var stringToOutputDisplay;
  var resultOfComparing1st, resultOfComparing2nd;

  for(i = startPosition; i < rowLength + startPosition - 1; i++)
  {
    // move the vertical screen.
    setScreenVerticalScrollWindow(panel, i, rowLength);
    // Search for the a cell element in the table, retrieve the value and converted to float.
    arraySearch = new Array(columnNumber ,i);
    cellRef = tableRef.FindChild(arrayPropName,arraySearch);
    // Search for the next cell element in the table, retrieve the value and converted to float.
    arraySearch = new Array(columnNumber ,i+1);
    nextCellRef = tableRef.FindChild(arrayPropName,arraySearch);
  
    // Verify which one is, string, number, date.
    resultOfComparing1st = findStringPattern(cellRef.contentText);
    resultOfComparing2nd = findStringPattern(nextCellRef.contentText);
  
    // Verify what it is to display the correct value into the log.
    if((resultOfComparing1st == 4 && resultOfComparing2nd == 4) || exception == "Last Ship")
      stringToOutputDisplay = "Date ";
    else if((resultOfComparing1st == 3 && resultOfComparing2nd == 3) || exception == "Farm ID" || exception == "Zone")
      stringToOutputDisplay = "String ";
    else if(resultOfComparing1st <= 2 && resultOfComparing2nd <= 2)
      stringToOutputDisplay = "Number ";
    else
      stringToOutputDisplay = "Weird type of number ";
  
    // Call the function which compare.
    compareResult = compareCellObjects(cellRef.contentText, nextCellRef.contentText, exception)
  
    if(verifAction == false) // Ascending
    {
      if(compareResult == 0 || compareResult == 2) // Greater than or equal.
      {
           // Rest -1 only for display the result due the cell offset.
           Log.checkpoint(stringToOutputDisplay + cellRef.contentText +" of Cell " + (i) + " and  "  + stringToOutputDisplay 
           + nextCellRef.contentText  + " of Cell " + (i+1) + " are equal or in an ascending to descending manner");
      }
      else
      {
           // Rest -1 only for display the result due the cell offset.
           Log.Error(stringToOutputDisplay + cellRef.contentText +" of Cell " + (i) + " and " + stringToOutputDisplay 
           + nextCellRef.contentText  + " of Cell " + (i+1) + " are NOT equal or in NOT an ascending to descending manner"); // list is sorted badly.
      }
    }
    else // Descending
    {
      if(compareResult == 1 || compareResult == 2) // Less than or equal.
      {
        // Rest -1 only for display the result due the cell offset.
        Log.checkpoint(stringToOutputDisplay + cellRef.contentText +" of Cell " + (i) + " and " + stringToOutputDisplay 
        + nextCellRef.contentText  + " of Cell " + (i+1) + " are equal or in a descending to ascending manner");
      }
      else
      {
        // Rest -2 only for display the result due the cell offset.
        Log.Error(stringToOutputDisplay + cellRef.contentText +" of Cell " + (i) + " and " + stringToOutputDisplay
        + nextCellRef.contentText  + " of Cell " + (i+1) + " are NOT equal or in NOT a descending to ascending manner"); // list is sorted badly.
      }
    }
  }
 return;
}

// Function to find the patter of the string
function findStringPattern(inputString)
{
  // Just Numbers with or without commas
  if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 0;
  // Return "$50". like (-or not -)$xx__.xx__ and with or without commas
  else if(inputString.search(/^-?\$[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 1;
    // Return "%" in percentage, like (-or not -)xx__.xx__% wit or without commas
  else if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?%+$/) != -1)
    return 2;
  else if(inputString.search(/^([1-9]|1[012])[/.]([1-9]|[12][0-9]|3[01])[/.](19|20)[0-9][0-9]$/) != -1)
    return 4;
  // Could be a String, only option left.
  else
    return 3;
}
// Function in charge of split the string of a cell depending of the result
// of which pattern is.
// Return: in string in case it is a string, no spli is made into strings
//         or floating - according to javascript all numbers are represented
//         in 64-bit floating points, decimals, integers, etc.
function splitStringBasePattern(inputString)
{
  var whichPattern;
  var resultString = "";
  var result;
  // Call the pattern function to verify how to split.
  whichPattern = findStringPattern(inputString);
  
  switch(whichPattern)
  {
    case 0:
    // Just decimals, remove commas
      resultString = inputString.replace(/\,/g, "");
      result = parseFloat(resultString);
      break;
    case 1:
    // Remove "$" and commas.
      resultString = inputString.replace(/\,|\$/g, "");
      result = parseFloat(resultString);
      break;
    case 2:
    // Remove "%" and commas
      resultString = inputString.replace(/\,|\%/g, "");
      result = parseFloat(resultString);
        break;
    // Do nothing in case of a string
    case 3:
      result = inputString;
      break;
    case 4:
      result = inputString.split("/");
      break;
    // Do nothing in case of a String
    default:
      result = inputString;
  }
  return result;
}

// Function to move the horizontal scroll of the cell panel.
function setScreenHorizontalScrollWindow(panel, columnCounter, numberOfTotalColumns)
{
  var panelIncreaseRate;
  // Go to the end when is the last column.
  if (columnCounter == numberOfTotalColumns)
  {
    panel.panel.scrollLeft = panel.panel.scrollWidth;
  }
  else
  {
    // Set the variable to move the panel every column iteration
    panelIncreaseRate = parseInt((panel.panel.scrollWidth) / (numberOfTotalColumns+1));
    // Move the screen.
    panel.panel.scrollLeft = panelIncreaseRate * columnCounter;
  }
}

// Function to move the vertical scroll of the cell panel.
function setScreenVerticalScrollWindow(panel, rowCounter, lenOfRows)
{
  var panelIncreaseRate;
  // Go to the end when is the last column.
  if (rowCounter == lenOfRows)
  {
    panel.panel.scrollTop = panel.panel.scrollHeight;
  }
  else
  {
    // Set the variable to move the panel every column iteration
    panelIncreaseRate = parseInt((panel.panel.scrollHeight - panel.panel.offsetTop) / (lenOfRows));
    // Move the screen.
    panel.panel.scrollTop = panelIncreaseRate * rowCounter;
  }
}

