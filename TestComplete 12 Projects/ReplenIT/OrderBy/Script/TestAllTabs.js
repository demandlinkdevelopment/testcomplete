﻿function Test1()
{
  var page;
  var form;
  var textbox;
  var panel;
  // Login information variables.
  var addr_ReplenIT = "https://replen3.demandlink.com/Home/Login";
  var userName = "testreplenityoungs";
  var password = "Replenit123";
  var howManyRows = 10; // options 10, 50, 100, 500, 1000 and 2000.
  var defaultOrAllColumns = true; // true: default columns, false: all columns.
    
  // Start Test:
  // - Open Browser in the webpage
  // - Login and wait.
  Browsers.Item(btChrome).Navigate(addr_ReplenIT);
  page = Aliases.browser.pageReplen3DemandlinkComHomeLogo;
  form = page.form;
  textbox = form.textboxUsername;
  textbox.SetText(userName);
  textbox.Keys("[Tab]");
  form.passwordboxPassword.SetText(password);
  textbox.Keys("[Tab]");
  form.submitbuttonLogin.Keys("[Enter]");
  page.Wait();
  panel = page.panelEdittableWrapper.panel.panel;
  page.WaitChild(panel.FullName);
  //page.Wait();
  
  // Variables to keep track of the values.
  var numColumns, tableLen;
  var arrayPropName, arrayPropValue;
  var topTable;
  var listOfColumns = [], individualColumn;
  var rowNumber;
  var isAscendingChecked = false;
  var isDescendingChecked = false;
  var cellOfTheTable;
  var firstSampleStr, secondSampleStr;
  var firstSampleResult, secondSampleResult;
  
  // Selects how many rows.
  page.panelMain.Click(65,142);
  page.Wait();
  if(howManyRows == 10)
  {
    page.panelRows.Click(74, 18);
  }
  else if(howManyRows == 50)
  {
    page.panelRows.Click(74, 55);
  }
  else if(howManyRows == 100)
  {
    page.panelRows.Click(74, 92);
  }
  else if(howManyRows == 500)
  {
    page.panelRows.Click(74, 120);
  }
  else if(howManyRows == 1000)
  {
    page.panelRows.Click(74, 150);
  }
  else if(howManyRows == 2000)
  {
    page.panelRows.Click(74, 190);
  }
  else // 10 rows
  {
    page.panelRows.Click(74, 18);
  }
  page.Wait();

  // Select default or all columns to be verified.
  page.panelMain.Click(193, 148);
  page.Wait();
  if(defaultOrAllColumns)
  {
    page.panelRows.Click(72, 76);
  }
  else
  {
    page.panelRows.Click(72, 42);
  }
  page.Wait();
  // Go out of the column menu by clicking outside.
  page.panelMain.Click(207, 94);
  page.Wait();
  
  // Number of the columns of the top of the table.
  // less 2 due the first column and because is zero based counting.
  // Consider the checkbox column.
  numColumns = (panel.table.ChildCount / panel.table.RowCount)-2;
  // Rows of the cell table (Seems to have two hidden elements):
  tableLen = panel.tableEdittable.RowCount;

  // Go out if there is no element in the table.
  if(tableLen <= 2 || numColumns < 0)
  {
    Log.Message("No Elements to compare in the table.");
    return;
  }
   
  // Start the panel screen from the beginning in case is not.
  panel.panel.scrollLeft = 0;
  panel.panel.scrollTop = 0;
  
  // Save all the columns in the array
  topTable = panel.table;
  arrayPropName = new Array("ColumnIndex", "RowIndex");
  rowNumber = 0; // Row for the title of each column
  for(i = 0; i <= numColumns; i++)
  {
    arrayPropValue = new Array(i+1, rowNumber);
    individualColumn = topTable.FindChild(arrayPropName, arrayPropValue);
    listOfColumns.push(individualColumn);
  }
  
  // Pointer to all table of the cells.
  allCellsTableEditable = panel.tableEdittable;
  var compareCellObjectResults;
  //var columnCounter = 10;
  for(columnCounter = 0; columnCounter <= numColumns; columnCounter++)
  {
    panel.panel.scrollTop = 0;
    // Restart tracking variables.
    isAscendingChecked = false;
    isDescendingChecked = false;
    // Retrive the Object Column of the top table List.
    //individualColumn = listOfColumns[columnCounter];
    individualColumn = listOfColumns.shift();
    // Click the column to be sorted the first time.
    individualColumn.Click();
    // Set the horizontal screen.
    setScreenHorizontalScrollWindow(panel, columnCounter, numColumns);
    page.WaitChild(panel.table.FullName);
    
    // Print the current column to ve verified
    Log.message("Testing \"" + individualColumn.contentText + "\" Column.");
    // Search for the first cell value of the column.
    rowNumber = 2;
    arrayPropValue = new Array(columnCounter+1, rowNumber);
    cellOfTheTable = allCellsTableEditable.FindChild(arrayPropName, arrayPropValue);
    
    // Do it ascending to descending and descending to ascending or backwards.
    do{
      // First sample of the value to identify in which status is: ascending or descending.
      firstSampleStr = cellOfTheTable.contentText;
      // Click the column to be sorted the next time and see which is the current status.
      individualColumn.Click();
      page.WaitChild(panel.table.FullName);
      // second sample of the value to identify in which status is: ascending or descending depending on first sample.
      secondSampleStr = cellOfTheTable.contentText;

      // Request if the object is greater, less or equal than depending of the type of string
      // that could be a Date, String or number.
      compareCellObjectResults = compareCellObjects(firstSampleStr, secondSampleStr, individualColumn.contentText);

      // Check if the status is from descending to ascending and if it was not checked before.
      if((compareCellObjectResults == 0) && (!isDescendingChecked))
      {
        Log.Message("Testing \"" + individualColumn.contentText + "\" Column for Descending to Ascending.");
        selectorOfVerifFunc(allCellsTableEditable , 2, tableLen, true, arrayPropName, 
        columnCounter+1,firstSampleStr, individualColumn.contentText, panel); // Descending
        isDescendingChecked = true; // Descending is checked.
      }
      // Check if the status is from ascending to descending and if it was not checked before.
      else if((compareCellObjectResults == 1) && (!isAscendingChecked))
      {
        Log.Message("Testing \"" + individualColumn.contentText + "\" Column for Ascending to Descending.");
        selectorOfVerifFunc(allCellsTableEditable , 2, tableLen, false, arrayPropName, 
        columnCounter+1,firstSampleStr, individualColumn.contentText, panel); // Ascending
        isAscendingChecked = true;
      }
      else
      {
         // Equals - all the elements are the same.
         Log.checkpoint("All the Elements of the \"" + individualColumn.contentText + "\" are equal.");
         isAscendingChecked = true;
         isDescendingChecked = true;
      }
    }while(!isDescendingChecked || !isAscendingChecked);
  
  }
  
  // Go back to default of displaying 10 rows.
  page.panelMain.Click(65, 142);
  page.Wait();
  page.panelRows.Click(74, 18);
  page.Wait();
  // Go to default columns once test finishs.
  page.panelMain.Click(193, 148);
  page.Wait();
  page.panelRows.Click(72, 76);
  page.Wait();
  page.panelMain.Click(207, 94);
  page.Wait();
  // End of Test.
  Log.Message("Test of OrderBy completed.")
}

// Function decide which function to execute depending if is string or number.
// Why? because the String function does not parse the value of the cell.
function selectorOfVerifFunc(tableRef, startPosition, rowLength, verifAction, arrayPropName, columnNumber, strPatternDec, except, panel)
{
  var typeOfInfoInCell = findStringPattern(strPatternDec);
  
  if(typeOfInfoInCell == 4 || except == "Last Ship")// date string
  // Handle an Exception in a specific column, this is because "Farm ID" handles "0" when is not ID
  //                                                           "Zone" handles "-" when is not Info.
    verifyAscOrDesDate(allCellsTableEditable , startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel);
  else if(typeOfInfoInCell == 3 || except == "Farm ID" || except == "Zone")
    verifyAscOrDesStrings(allCellsTableEditable , startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel);
  else
    verifyAscOrDesNumbers(allCellsTableEditable , startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel);
  return;
}

// This function compares if the object is greater, less or equal depending on the string colected from the cell
// Return:
//  0 - if the first element is greater than the second
//  1 - if the first element is less than the second.
// 2 if both are equal.
function compareCellObjects(firstCellObjectStr, secondCellObjectStr, exception)
{
  // Verify which one is, string, number, date.
  var resultOfComparing1st = findStringPattern(firstCellObjectStr);
  var resultOfComparing2nd = findStringPattern(secondCellObjectStr);
  // Create variables to track and compare.
  var firstCellObjectInt, secondCellObjectInt;
  var firstCellDateArray, secondCellDateArray;
  var firstCellObjectDate, secondCellObjectDate;
  
  // Compare Dates.
  if((resultOfComparing1st == 4 && resultOfComparing2nd == 4) || exception == "Last Ship")
  {
    // Create the dates according the split pattern.
    firstCellObjectDate = new Date();
    //firstCellDateArray = splitStringBasePattern(firstCellObjectStr);
    //firstCellObjectDate.setFullYear(parseInt(firstCellDateArray[2]),parseInt(firstCellDateArray[0])-1, parseInt(firstCellDateArray[1]));
    if(firstCellObjectStr == "")
    {
      firstCellObjectDate.setFullYear(0,0,0);
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    else
    {
      firstCellDateArray = splitStringBasePattern(firstCellObjectStr);
      firstCellObjectDate.setFullYear(parseInt(firstCellDateArray[2]),parseInt(firstCellDateArray[0])-1, parseInt(firstCellDateArray[1]));
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    
    secondCellObjectDate = new Date();
    //secondCellDateArray = splitStringBasePattern(secondCellObjectStr);
    //secondCellObjectDate.setFullYear(parseInt(secondCellDateArray[2]),parseInt(secondCellDateArray[0])-1, parseInt(secondCellDateArray[1]));
    if(secondCellObjectStr == "")
    {
      secondCellObjectDate.setFullYear(0,0,0);
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    else
    {
      secondCellDateArray = splitStringBasePattern(secondCellObjectStr);
      secondCellObjectDate.setFullYear(parseInt(secondCellDateArray[2]),parseInt(secondCellDateArray[0])-1, parseInt(secondCellDateArray[1]));
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    // Compare:
    if(firstCellObjectDate.getTime() > secondCellObjectDate.getTime())
      return 0; // it is greater.
    else if(firstCellObjectDate.getTime() < secondCellObjectDate.getTime())
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare Strings.
  // Due this exception has a number when is no info in it, consider this as string
  else if((resultOfComparing1st == 3 && resultOfComparing2nd == 3) || exception == "Farm ID" || exception == "Zone")
  {
    // No need to transform, work directly with strings.
    // Compare:
    if(firstCellObjectStr > secondCellObjectStr)
      return 0; // it is greater.
    else if(firstCellObjectStr < secondCellObjectStr)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare numbers.
  else if(resultOfComparing1st <= 2 && resultOfComparing2nd <= 2)
  {
    firstCellObjectInt = parseFloat(splitStringBasePattern(firstCellObjectStr));
    secondCellObjectInt = parseFloat(splitStringBasePattern(secondCellObjectStr));
    // Compare:
    if(firstCellObjectInt > secondCellObjectInt)
      return 0; // it is greater.
    else if(firstCellObjectInt < secondCellObjectInt)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  else
    Log.Error("Elements in the Cells are not the same type or has the exception.")
}
// Function to verify if it list is ascending or descending by "only" numbers, 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
function verifyAscOrDesNumbers(tableRef, startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel)
{
 var arraySearch;
 var cellRef, nextCellRef;
 var readCellValueStr = "", readNextCellValueStr = "";
 var readCellValueInt = 0, readNextCellValueInt = 0; 
 
 for(i = startPosition; i < (rowLength-1); i++)
 {
   // move the vertical screen.
   setScreenVerticalScrollWindow(panel, i, rowLength-2);
   // Search for the a cell element in the table, retrieve the value and converted to float.
   arraySearch = new Array(columnNumber ,i);
   cellRef = tableRef.FindChild(arrayPropName,arraySearch);
   readCellValueStr = splitStringBasePattern(cellRef.contentText);
   readCellValueInt = parseFloat(readCellValueStr);
   
   // Search for the next cell element in the table, retrieve the value and converted to float.
   arraySearch = new Array(columnNumber ,i+1);
   nextCellRef = tableRef.FindChild(arrayPropName,arraySearch);
   readNextCellValueStr = splitStringBasePattern(nextCellRef.contentText);
   readNextCellValueInt = parseFloat(readNextCellValueStr);
      
   if(verifAction == false) // Ascending
   {
     if(readCellValueInt >= readNextCellValueInt)
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("Value " + cellRef.contentText +" of Cell " + (i-1) + " and Value " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are equal or in an ascending to descending manner");
     }
     else
     {
       // Rest -1 only for display the result due the cell offset.
       Log.Error("Value " + cellRef.contentText +" of Cell " + (i-1) + " and Value " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are NOT equal or in NOT an ascending to descending manner"); // list is sorted badly.
     }
   }
   else // Descending
   {
     if(readCellValueInt <= readNextCellValueInt)
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("Value " + cellRef.contentText +" of Cell " + (i-1) + " and Value " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are equal or in a descending to ascending manner");
     }
     else
     {
       // Rest -2 only for display the result due the cell offset.
       Log.Error("Value " + cellRef.contentText +" of Cell " + (i-1) + " and Value " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are NOT equal or in NOT a descending to ascending manner"); // list is sorted badly.
     }
   }
 }
 return;
}

// Function to verify if it list is ascending or descending by "only" strings values, 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
function verifyAscOrDesStrings(tableRef, startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel)
{
 var arraySearch;
 var cellRef, nextCellRef;
 var readCellValueStr = "", readNextCellValueStr = "";
 
 for(i = startPosition; i < (rowLength-1); i++)
 {
   // move the vertical screen.
   setScreenVerticalScrollWindow(panel, i, rowLength-2);
   // Search for the a cell element in the table, retrieve the value and converted to int.
   arraySearch = new Array(columnNumber ,i);
   cellRef = tableRef.FindChild(arrayPropName,arraySearch);
   readCellValueStr = cellRef.contentText;
   
   // Search for the next cell element in the table, retrieve the value and converted to int.
   arraySearch = new Array(columnNumber ,i+1);
   nextCellRef = tableRef.FindChild(arrayPropName,arraySearch);
   readNextCellValueStr = nextCellRef.contentText;
      
   if(verifAction == false) // Ascending
   {
     if(readCellValueStr >= readNextCellValueStr)
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("String " + cellRef.contentText +" of Cell " + (i-1) + " and String " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are equal or in an ascending to descending manner");
     }
     else
     {
       // Rest -1 only for display the result due the cell offset.
       Log.Error("String " + cellRef.contentText +" of Cell " + (i-1) + " and String " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are NOT equal or in NOT an ascending to descending manner"); // list is sorted badly.
     }
   }
   else // Descending
   {
     if(readCellValueStr <= readNextCellValueStr)
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("String " + cellRef.contentText +" of Cell " + (i-1) + " and String " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are equal or in a descending to ascending manner");
     }
     else
     {
       // Rest -1 only for display the result due the cell offset.
       Log.Error("String " + cellRef.contentText +" of Cell " + (i-1) + " and String " 
       + nextCellRef.contentText  + " of Cell " + (i+1-1) + " are NOT equal or in NOT a descending to ascending manner"); // list is sorted badly.
     }
   }
 }
 return;
}

// Function to verify if it list is ascending or descending by "only" Dates, 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
function verifyAscOrDesDate(tableRef, startPosition, rowLength, verifAction, arrayPropName, columnNumber, panel)
{
 var arraySearch;
 var cellRef, nextCellRef;
 var readCellValueDate, readNextCellValueDate;
 var readCellValueArray = [], readNextCellValueArray = [];
 
 for(i = startPosition; i < (rowLength-1); i++)
 {
   // move the vertical screen.
   setScreenVerticalScrollWindow(panel, i, rowLength-2);
   // Search for the a cell element in the table, retrieve the value and converted to Date.
   arraySearch = new Array(columnNumber ,i);
   cellRef = tableRef.FindChild(arrayPropName,arraySearch);
   
   readCellValueDate = new Date();
   if(cellRef.contentText == "")
   {
     readCellValueDate.setFullYear(0,0,0);
     readCellValueDate.setHours(0); readCellValueDate.setMilliseconds(0); 
     readCellValueDate.setMinutes(0); readCellValueDate.setSeconds(0);
   }
   else
   {
     // Set day according the split.
     readCellValueArray = splitStringBasePattern(cellRef.contentText);
     readCellValueDate.setFullYear(parseInt(readCellValueArray[2]),parseInt(readCellValueArray[0])-1, parseInt(readCellValueArray[1]));
     readCellValueDate.setHours(0); readCellValueDate.setMilliseconds(0); 
     readCellValueDate.setMinutes(0); readCellValueDate.setSeconds(0);
   }
   
   // Search for the next cell element in the table, retrieve the value and converted to Date.
   arraySearch = new Array(columnNumber ,i+1);
   nextCellRef = tableRef.FindChild(arrayPropName,arraySearch);
   
   readNextCellValueDate = new Date();
   if(nextCellRef.contentText == "")
   {
      readNextCellValueDate.setFullYear(0,0,0);
     readNextCellValueDate.setHours(0); readNextCellValueDate.setMilliseconds(0); 
     readNextCellValueDate.setMinutes(0); readNextCellValueDate.setSeconds(0);
   }
   else
   {
     // Set day according the split.
     readNextCellValueArray = splitStringBasePattern(nextCellRef.contentText);
     readNextCellValueDate.setFullYear(parseInt(readCellValueArray[2]),parseInt(readCellValueArray[0])-1, parseInt(readCellValueArray[1]));
     readNextCellValueDate.setHours(0); readNextCellValueDate.setMilliseconds(0); 
     readNextCellValueDate.setMinutes(0); readNextCellValueDate.setSeconds(0);
   }
   
   if(verifAction == false) // Ascending
   {
     if(readCellValueDate.getTime() >= readNextCellValueDate.getTime())
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("Date " + cellRef.contentText + " of Cell " + (i-1) + " and Date " 
       + nextCellRef.contentText + " of Cell " + (i+1-1) + " are equal or in an ascending to descending manner");
     }
     else
     {
       // Rest -1 only for display the result due the cell offset.
       Log.Error("Date " + cellRef.contentText + " of Cell " + (i-1) + " and Date " 
       + nextCellRef.contentText + " of Cell " + (i+1-1) + " are NOT equal or NOT in an ascending to descending manner");// sorted badly.
     }
   }
   else // Descending
   {
     if(readCellValueDate.getTime() <= readNextCellValueDate.getTime())
     {
       // Rest -1 only for display the result due the cell offset.
       Log.checkpoint("Date " + cellRef.contentText + " of Cell " + (i-1) + " and Date " 
       + nextCellRef.contentText + " of Cell " + (i+1-1) + " are equal or in an descending to ascending manner");
     }
     else
     {
       // Rest -1 only for display the result due the cell offset.
       Log.Error("Date " + cellRef.contentText + " of Cell " + (i-1) + " and Date " 
       + nextCellRef.contentText + " of Cell " + (i+1-1) + " are NOT equal or NOT in an descending to ascending manner"); // sorted badly.
     }
   }
 }
 return;
}

// Function to find the patter of the string
function findStringPattern(inputString)
{
  // Just Numbers with or without commas
  if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 0;
  // Return "$50". like (-or not -)$xx__.xx__ and with or without commas
  else if(inputString.search(/^-?\$[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 1;
    // Return "%" in percentage, like (-or not -)xx__.xx__% wit or without commas
  else if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?%+$/) != -1)
    return 2;
  else if(inputString.search(/^([1-9]|1[012])[/.]([1-9]|[12][0-9]|3[01])[/.](19|20)[0-9][0-9]$/) != -1)
    return 4;
  // Could be a String, only option left.
  else
    return 3;
}
// Function in charge of split the string of a cell depending of the result
// of which pattern is.
// Return: in string in case it is a string, no spli is made into strings
//         or floating - according to javascript all numbers are represented
//         in 64-bit floating points, decimals, integers, etc.
function splitStringBasePattern(inputString)
{
  var whichPattern;
  var resultString = "";
  var result;
  // Call the pattern function to verify how to split.
  whichPattern = findStringPattern(inputString);
  
  switch(whichPattern)
  {
    case 0:
    // Just decimals, remove commas
      resultString = inputString.replace(/\,/g, "");
      result = parseFloat(resultString);
      break;
    case 1:
    // Remove "$" and commas.
      resultString = inputString.replace(/\,|\$/g, "");
      result = parseFloat(resultString);
      break;
    case 2:
    // Remove "%" and commas
      resultString = inputString.replace(/\,|\%/g, "");
      result = parseFloat(resultString);
        break;
    // Do nothing in case of a string
    case 3:
      result = inputString;
      break;
    case 4:
      result = inputString.split("/");
      break;
    // Do nothing in case of a String
    default:
      result = inputString;
  }
  return result;
}

// Function to move the horizontal scroll of the cell panel.
function setScreenHorizontalScrollWindow(panel, columnCounter, numberOfTotalColumns)
{
  var panelIncreaseRate;
  // Go to the end when is the last column.
  if (columnCounter == numberOfTotalColumns)
  {
    panel.panel.scrollLeft = panel.panel.scrollWidth;
  }
  else
  {
    // Set the variable to move the panel every column iteration
    panelIncreaseRate = parseInt((panel.panel.scrollWidth) / (numberOfTotalColumns+1));
    // Move the screen.
    panel.panel.scrollLeft = panelIncreaseRate * columnCounter;
  }
}

// Function to move the vertical scroll of the cell panel.
function setScreenVerticalScrollWindow(panel, rowCounter, lenOfRows)
{
  var panelIncreaseRate;
  // Go to the end when is the last column.
  if (rowCounter == lenOfRows)
  {
    panel.panel.scrollTop = panel.panel.scrollHeight;
  }
  else
  {
    // Set the variable to move the panel every column iteration
    panelIncreaseRate = parseInt((panel.panel.scrollHeight - panel.panel.offsetTop) / (lenOfRows));
    // Move the screen.
    panel.panel.scrollTop = panelIncreaseRate * rowCounter;
  }
}

