﻿function Test1()
{
  var page;
  var form;
  var textbox;
  var panel;
  // Address variable.
  var addr_ReplenIT = "https://replen3.demandlink.com/Home/Login";
  var userName = "testreplenityoungs";
  var password = "Replenit123";
  var cellStoreID, cellItemID;
  var numberOfRowsList;
  // Variables to keep track of the values.
  var firstSampleStr;
  var firstSampleInt;
  var secondSampleStr;
  var secondSampleInt;
  var tableLen = 0;
  var isAscendingChecked = false;
  var isDescendingChecked = false;
  
  // Start Test:
  // - Open Browser in the webpage
  // - Login and wait.
  Browsers.Item(btChrome).Navigate(addr_ReplenIT);
  page = Aliases.browser.pageReplen3DemandlinkComHomeLogo;
  form = page.form;
  textbox = form.textboxUsername;
  textbox.SetText(userName);
  textbox.Keys("[Tab]");
  form.passwordboxPassword.SetText(password);
  textbox.Keys("[Tab]");
  form.submitbuttonLogin.Keys("[Enter]");
  page.Wait();
  panel = page.panelEdittableWrapper.panel.panel;
  page.Wait();
      
  // Length of the table (Seems to have two hidden elements):
  tableLen = panel.tableEdittable.RowCount;
  // Go out if there is no element in the table.
  if(tableLen <= 2)
  {
    Log.Message("No Elements to compare in the table.");
    return;
  }
  
  // Selects 500 in a row.
  //page.panelEdittableWrapper.link.Click();
  //page.link.Click();
  //page.Wait();
  
  // Pointer to all table of the cells.
  allCellsTableEdditable = panel.tableEdittable;
  
  // ************************************
  // Verify first tab (StoreID)
  // ************************************
  Log.Message("Testing StoreID Column.");
  var strColumnNumber = "1";
  var stringPropNameStoreID = "Name";
  var stringPropValueStoreID = "Cell";
  
  // cell for StoreID - selector for ascending or descending
  cellStoreID = panel.table.cell;
  // Click to sort first time in case it's not sorted.
  cellStoreID.Click();
  page.Wait();
  // First cell reference of the StoreID column
  firstStoreIDCell = panel.tableEdittable.cell;
  
  // Check the status of the StoreID, if it is ascending to descending and descending to ascending.
  do{
      // Save the content of the first StoreID
      firstSampleStr = firstStoreIDCell.innerHTML;
      firstSampleInt = parseInt(firstSampleStr);
      // Click again the ascending and descending button.
      cellStoreID.Click();
      page.Wait();
      // Save the content of the second the opposite status StoreID
      secondSampleStr = firstStoreIDCell.innerHTML;
      secondSampleInt = parseInt(secondSampleStr);
      
      // Check if the status is from descending to ascending and if it was not checked before.
      if(((firstSampleInt - secondSampleInt) > 0) && (!isDescendingChecked))
      {
        Log.Message("Testing StoreID Column for Descending to Ascending.");
        verifyAscOrDesNumbers(allCellsTableEdditable , 2, tableLen, true, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Descending
        isDescendingChecked = true; // Descending is checked.
      }
      // Check if the status is from ascending to descending and if it was not checked before.
      else if(((firstSampleInt - secondSampleInt) < 0) && (!isAscendingChecked))
      {
        Log.Message("Testing StoreID Column for Ascending to Descending.");
        verifyAscOrDesNumbers(allCellsTableEdditable , 2, tableLen, false, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Ascending
        isAscendingChecked = true;
      }
      else
      {
         // Equals - all the elements are the same.
         Log.checkpoint("All the Elements of the Table are equal.");
      }
  }while(!isDescendingChecked || !isAscendingChecked);
  
  // ************************************
  // Verify second tab (Store Desc)
  // ************************************
  Log.Message("Testing Store Desc Column.");
  var strColumnNumber = "2";
  var stringPropNameStoreID = "Name";
  var stringPropValueStoreID = "Cell";
  // Restart variables
  isAscendingChecked = false;
  isDescendingChecked = false;
  
  // cell for StoreID - selector for ascending or descending
  cellStoreDesc = panel.table.cell2;
  // Click to sort first time in case it's not sorted.
  cellStoreDesc.Click();
  page.Wait();
  // First cell reference of the StoreID column
  firstStoreIDCell = panel.tableEdittable.cell2;
  
  // Check the status of the StoreID, if it is ascending to descending and descending to ascending.
  do{
      // Save the content of the first Store Desc
      firstSampleStr = firstStoreIDCell.innerHTML;
      // firstSampleInt = parseInt(firstSampleStr);
      // Click again the ascending and descending button.
      cellStoreDesc.Click();
      page.Wait();
      // Save the content of the second the opposite status StoreID
      secondSampleStr = firstStoreIDCell.innerHTML;
      //secondSampleInt = parseInt(secondSampleStr);
      
      // Check if the status is from descending to ascending and if it was not checked before.
      if(((firstSampleStr > secondSampleStr)) && (!isDescendingChecked))
      {
        Log.Message("Testing Store Desc Column for Descending to Ascending.");
        verifyAscOrDesStrings(allCellsTableEdditable , 2, tableLen, true, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Descending
        isDescendingChecked = true; // Descending is checked.
      }
      // Check if the status is from ascending to descending and if it was not checked before.
      else if(((firstSampleStr < secondSampleStr)) && (!isAscendingChecked))
      {
        Log.Message("Testing Store Desc Column for Ascending to Descending.");
        verifyAscOrDesStrings(allCellsTableEdditable , 2, tableLen, false, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Ascending
        isAscendingChecked = true;
      }
      else
      {
         // Equals - all the elements are the same.
         Log.checkpoint("All the Elements of the Table are equal.");
      }
  }while(!isDescendingChecked || !isAscendingChecked);
  
  // ************************************
  // Verify third tab (Item ID)
  // ************************************
  Log.Message("Testing ItemID Column.");
  var strColumnNumber = "3";
  var stringPropNameStoreID = "Name";
  var stringPropValueStoreID = "Cell";
  
  // Restart variables
  isAscendingChecked = false;
  isDescendingChecked = false;
  
  // cell for StoreID - selector for ascending or descending
  cellItemID = panel.table.cell3;
  // Click to sort first time in case it's not sorted.
  cellItemID.Click();
  page.Wait();
  // First cell reference of the ItemID column
  firstItemIDCell = panel.tableEdittable.cell3;
  // Check the status of the ItemID, if it is ascending to descending and descending to ascending.
  do{
      // Save the content of the first ItemID
      firstSampleStr = firstItemIDCell.innerHTML;
      firstSampleInt = parseInt(firstSampleStr);
      // Click again the ascending and descending button.
      cellItemID.Click();
      page.Wait();
      // Save the content of the second the opposite status ItemID
      secondSampleStr = firstItemIDCell.innerHTML;
      secondSampleInt = parseInt(secondSampleStr);
      
      // Check if the status is from descending to ascending and if it was not checked before.
      if(((firstSampleInt - secondSampleInt) > 0) && (!isDescendingChecked))
      {
        Log.Message("Testing ItemID Column for Descending to Ascending.");
        verifyAscOrDesNumbers(allCellsTableEdditable , 2, tableLen, true, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Descending
        isDescendingChecked = true; // Descending is checked.
      }
      // Check if the status is from ascending to descending and if it was not checked before.
      else if(((firstSampleInt - secondSampleInt) < 0) && (!isAscendingChecked))
      {
        Log.Message("Testing ItemID Column for Ascending to Descending.");
        verifyAscOrDesNumbers(allCellsTableEdditable , 2, tableLen, false, stringPropNameStoreID, stringPropValueStoreID, strColumnNumber); // Ascending
        isAscendingChecked = true;
      }
      else
      {
         // Equals - all the elements are the same.
         Log.checkpoint("All the Elements of the Table are equal.");
      }
  }while(!isDescendingChecked || !isAscendingChecked);
}

// Function to verify if it list is ascending or descending by only checking number values (not strings), 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
function verifyAscOrDesNumbers(tableRef, startPosition, rowLength, verifAction, strPropName, strPropValues, strColumnNumber)
{
 var strSearch = "";
 var cellRef, nextCellRef;
 var readCellValueStr = "", readNextCellValueStr = "";
 var readCellValueInt = 0, readNextCellValueInt = 0; 
 
 for(i = startPosition; i < (rowLength-1); i++)
 {
   // Search for the a cell element in the table, retrieve the value and converted to int.
   strSearch = strPropValues + "(" + i + ", " + strColumnNumber + ")";
   cellRef = tableRef.FindChild(strPropName,strSearch);
   readCellValueStr = cellRef.innerHTML;
   readCellValueInt = parseInt(readCellValueStr);
   
   // Search for the next cell element in the table, retrieve the value and converted to int.
   strSearch = strPropValues + "(" + (i+1) + ", " + strColumnNumber + ")";
   nextCellRef = tableRef.FindChild(strPropName,strSearch);
   readNextCellValueStr = nextCellRef.innerHTML;
   readNextCellValueInt = parseInt(readNextCellValueStr);
      
   if(verifAction == false) // Ascending
   {
     if(readCellValueInt >= readNextCellValueInt)
     {
       Log.checkpoint("Value " + readCellValueInt +" of Cell " + i + " and Value " 
       + readNextCellValueInt  + " of Cell " + (i+1) + " are equal or in an ascending manner");
     }
     else
     {
       Log.Error("Value " + readCellValueInt +" of Cell " + i + " and Value " 
       + readNextCellValueInt  + " of Cell " + (i+1) + " are NOT equal or in NOT an ascending manner"); // list is sorted badly.
     }
   }
   else // Descending
   {
     if(readCellValueInt <= readNextCellValueInt)
     {
       Log.checkpoint("Value " + readCellValueInt +" of Cell " + i + " and Value " 
       + readNextCellValueInt  + " of Cell " + (i+1) + " are equal or in a descending manner");
     }
     else
     {
       Log.Error("Value " + readCellValueInt +" of Cell " + i + " and Value " 
       + readNextCellValueInt  + " of Cell " + (i+1) + " are NOT equal or in NOT a descending manner"); // list is sorted badly.
     }
   }
 }
 return;
}

// Function to verify if it list is ascending or descending by only checking strings values (not numbers), 
// tableRef : Reference of the table to be verified.
// startPosition : position where the values cell start.
// rowLength : number of the cells in the column.
// verifAction : false - ascending, :true - descending
// strPropName : Property value to search the cell in the table.
// strPropValue : value of the property to be searched.
// strColumnNumber : the number of the column.
function verifyAscOrDesStrings(tableRef, startPosition, rowLength, verifAction, strPropName, strPropValues, strColumnNumber)
{
 var strSearch = "";
 var cellRef, nextCellRef;
 var readCellValueStr = "", readNextCellValueStr = "";
 
 for(i = startPosition; i < (rowLength-1); i++)
 {
   // Search for the a cell element in the table, retrieve the value and converted to int.
   strSearch = strPropValues + "(" + i + ", " + strColumnNumber + ")";
   cellRef = tableRef.FindChild(strPropName,strSearch);
   readCellValueStr = cellRef.innerHTML;
   
   // Search for the next cell element in the table, retrieve the value and converted to int.
   strSearch = strPropValues + "(" + (i+1) + ", " + strColumnNumber + ")";
   nextCellRef = tableRef.FindChild(strPropName,strSearch);
   readNextCellValueStr = nextCellRef.innerHTML;
      
   if(verifAction == false) // Ascending
   {
     if(readCellValueStr >= readNextCellValueStr)
     {
       Log.checkpoint("Value " + readCellValueStr +" of Cell " + i + " and Value " 
       + readNextCellValueStr  + " of Cell " + (i+1) + " are equal or in an ascending manner");
     }
     else
     {
       Log.Error("Value " + readCellValueStr +" of Cell " + i + " and Value " 
       + readNextCellValueStr  + " of Cell " + (i+1) + " are NOT equal or in NOT an ascending manner"); // list is sorted badly.
     }
   }
   else // Descending
   {
     if(readCellValueStr <= readNextCellValueStr)
     {
       Log.checkpoint("Value " + readCellValueStr +" of Cell " + i + " and Value " 
       + readNextCellValueStr  + " of Cell " + (i+1) + " are equal or in a descending manner");
     }
     else
     {
       Log.Error("Value " + readCellValueStr +" of Cell " + i + " and Value " 
       + readNextCellValueStr  + " of Cell " + (i+1) + " are NOT equal or in NOT a descending manner"); // list is sorted badly.
     }
   }
 }
 return;
}