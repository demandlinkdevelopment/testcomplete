﻿// Include some functions.
var Utils_PKG = require("Utils_PKG");

function Test1()
{
  var page;
  var form;
  var textbox;
  var cellsPanel;
  // Login information variables.
  var addr_ReplenIT = "https://replen3.demandlink.com/Home/Login";
  var userName = "testreplenityoungs";
  var password = "Replenit123";
  var howManyRows = 10; // options 10, 50, 100, 500, 1000 and 2000.
  var defaultOrAllColumns = true; // true: default columns, false: all columns.
  // Selector of the top tabs: "Edit", "Stores", "Subregion", "Items", "FarmID" and "Exceptions".
  var topTabsSelectoStr = "Edit";
  // Avail Units = Available in other top tabs.
  //var listOfColumsToVerify = new Array("Weeks On Exception", "MaxWeeksOnException","Avail Units", "On Ad", "Available"); 
  
  // Variables to keep track of the values.
  var columnOffset, rowOffset;
  var numColumns, numRows, rowNumber;
  var topRowCellsTable;
  var listOfColumns = [], individualColumn;
  var listOfFilterColumns = [], individualFilterColumn;
  var arrayPropName = new Array("ColumnIndex", "RowIndex"), arrayPropValue;
  var panelGlobalSideNav, panelIncludeFilter;
  var numberOfFilterIter;
  var objectsOfFilterOptions = [];
  var mainFilterColumnWrapper;
  var filterXScroll, filterYTop;
  var currElementFilter, filterValue;
  var cellValue;
  var nextButtonStatus;
  
  // Start Test:
  // - Open Browser in the webpage
  // - Login and wait.
  Browsers.Item(btChrome).Navigate(addr_ReplenIT);
  page = Aliases.browser.pageReplen3DemandlinkComHomeLogo;
  form = page.form;
  textbox = form.textboxUsername;
  textbox.SetText(userName);
  textbox.Keys("[Tab]");
  form.passwordboxPassword.SetText(password);
  textbox.Keys("[Tab]");
  form.submitbuttonLogin.Keys("[Enter]");
  page.Wait();
  // Save the left side navigation panel.
  panelIncludeFilter = page.panelMysidenav.panelIncludeFilter;

  //panelGlobalSideNav = page.panelMysidenav.panelCollapseone.panel;
  page.Wait();
  
  // All the cells of the panel.
  cellsPanel = Utils_PKG.selectorTopTabs(topTabsSelectoStr, page);
  page.Wait();
  
  // Collapse Global Filter option
  page.panelMysidenav.Click(72, 100);
  page.Wait();
  // Expand Table Filter option
  page.panelMysidenav.Click(72, 150);
  page.Wait();
    
  // Selects how many rows.
  //Utils_PKG.selectsNumberOfRows(page, howManyRows);

  // Hide all the columns and selects only the
  // one are tested in this Include Filter test:
  // only the ones that a Include filter can be applied.
  // Only when the current tab is "Edit".
  //Utils_PKG.selectEditColumns(page, topTabsSelectoStr);
  
  // There is no CheckBox column in this Test.
  columnOffset = 1;
  // Edit seems to have one extra row in there.
  if(topTabsSelectoStr == "Edit")
    rowOffset = 2;
  else
    rowOffset = 1;

  // Number of columns.
  numColumns = (cellsPanel.table.ChildCount / cellsPanel.table.RowCount)-columnOffset;
  // Rows of the cell table (Seems to have two or one hidden elements):
  numRows = cellsPanel.tableAllCellObjects.RowCount - rowOffset;
  
  // Go out if there is no element in the table.
  if(numRows <= 1 || numColumns <= 0)
  {
    Log.Message("No Elements to compare in the table.");
    return;
  }
  // Start the panel screen from the beginning in case is not.
  cellsPanel.panel.scrollLeft = 0;
  cellsPanel.panel.scrollTop = 0;
  
  // Save all the columns in the array
  topRowCellsTable = cellsPanel.table;
  rowNumber = 0; // Row for the title of each column
  for(i = 0; i <= numColumns; i++)
  {
    // Save the column name.
    arrayPropValue = new Array(i+(columnOffset-1), rowNumber);
    individualColumn = topRowCellsTable.FindChild(arrayPropName, arrayPropValue);
    listOfColumns.push(individualColumn);
    // Save the reference to the option filter button below the column name.
    arrayPropValue = new Array(i+(columnOffset-1), rowNumber+1);
    individualFilterColumn = topRowCellsTable.FindChild(arrayPropName, arrayPropValue);
    listOfFilterColumns.push(individualFilterColumn);
  }

  for(columnCounter = 0; columnCounter <= numColumns; columnCounter++)
  {
    // Retrieve the column of the list.
    individualColumn = listOfColumns.shift();
    page.Wait();
    // Retrive the filter column of the list.
    individualFilterColumn = listOfFilterColumns.shift();
    page.Wait();
    
    // Click the Filter option if its correspond column.
    individualFilterColumn.Click(individualFilterColumn.Width/2, (individualFilterColumn.Height/2)-10);
    page.Wait();
    
    // Retrieve all the options in the Filter Node
    objectsOfFilterOptions = page.FindAllChildren("ObjectType", "TextNode");
    page.Wait();
    // Conver to Native Array.
    objectsOfFilterOptions = Array.from(objectsOfFilterOptions);
    // All Filter option main object.
    mainFilterColumnWrapper = objectsOfFilterOptions.shift();
    // Due the FindAllChildren recover the data in backwards reverse it.
    objectsOfFilterOptions.reverse();
    
    currElementFilter = 0;
    // Move the top scroll of the main filter wrapper.
    if (currElementFilter == 0)
      mainFilterColumnWrapper.scrollTop = 0;
    else
      mainFilterColumnWrapper.scrollTop = objectsOfFilterOptions[currElementFilter].offsetTop 
      - objectsOfFilterOptions[currElementFilter-1].offsetTop;
    page.Wait();
    // Save the value of the picked Filter value.
    filterValue = objectsOfFilterOptions[currElementFilter].contentText;
    // X = offsetWidth / 2 = in the middle of the main Filter colum.
    filterXScroll = objectsOfFilterOptions[currElementFilter].offsetWidth/2;
    // Y = offsetTop minus the height due it starts at botton left and is not well defined less the scroll value when the 
    // the windows of the list moves.
    filterYTop = (objectsOfFilterOptions[currElementFilter].offsetTop - objectsOfFilterOptions[currElementFilter].Height 
    - mainFilterColumnWrapper.scrollTop);
    // Select the choosen filter.
    mainFilterColumnWrapper.Click(filterXScroll, filterYTop);
    page.Wait();
    
    // Verify if the value of the filter selected appears in the data inside the side menu bar
    // ************ IMPROVE THIS SEARHC ********************//
    if(panelIncludeFilter.panelFilterBox.contentText.search(filterValue) != -1)
      Log.Message("The selected Include Filter value: \"" + filterValue + "\" found in the side panel bar.");
    else
      Log.Error("The selected Include Filter value: \"" + filterValue + "\" NOT found in the side panel bar.");
    
    // Check how many rows are after the filter is applied
    numRows = cellsPanel.tableAllCellObjects.RowCount - rowOffset;
    // if is empty, something wrong happen -- CLARIFY DOUBT: WHEN THERE IS A FILTER THERE SHOULD BE A VALUE IN THEIR DATA LIST?
    // Check if every element in the remaing data when the filter is applied is equal to the value of the filter.
    // No need to change the type of the data, can be compare as it is.
    
    
    // Check how many rows are after the filter is applied
    numRows = cellsPanel.tableAllCellObjects.RowCount - rowOffset;
    rowNumber = 2; // where the data starts in the cells.
    // Iterate for every value cell in the cells and verify that the value is in there.
    for(i = rowNumber; i <= numRows + rowNumber - 1; i++)
    {
      arrayPropValue= new Array(columnCounter+(columnOffset-1), i);
      cellValueObject = cellsPanel.tableAllCellObjects.FindChild(arrayPropName, arrayPropValue);
      cellValue = cellValueObject.contentText;
      // Should be only the values that are selecter for the include filter.
      if(filterValue == cellValue)
        Log.Message("The value for the selected filter \"" + filterValue + "\" is the same value \"" + cellValue + "\" as the Cell( "
        + (columnCounter+(columnOffset-1)) + ", " + i + ") for the column: " + individualColumn.contentText);
      else
        Log.Error("The value for the selected filter \"" + filterValue + "\" is NOT the same value \"" + cellValue + "\" as the Cell( "
        + (columnCounter+(columnOffset-1)) + ", " + i + ") for the column: " + individualColumn.contentText + " - SOMETHING HAPPENDS!!");
    }
    Utils_PKG.selectorNextButton(topTabsSelectoStr, page);
    return;
  }
  

  // Set the default rows and columns.
  // Utils_PKG.defaultRowAndColumns(page, topTabsSelectoStr);
  // End of Test.
  Log.Message("Test of Include Filters completed.")
  return;
}