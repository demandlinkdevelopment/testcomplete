﻿// numOfFIlterChilds: list element seems to be on the top of the page, give the number of them.
// wrapperOfElementList: it is depending on the filter, it is not TextNode(integer) it is TextNode(string),
//                       that string depends on the filter type.
// fieldCellMap: it is the map of the cells to verify if the content of the cell is correct when the filter
//               is applied, without the "Cell".
// x_cell: it is the index for the column.
function selectedFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldTableMap, x_cell, wrapperBodyPanel)
{
  specialFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldTableMap, x_cell, wrapperBodyPanel);
}
// This function search, selects, applied the filter and verify the content for filter list where the 
// height of the element of the filter is NOT constant and the elements to be verify is two values,
// this make the filter faster than the other.
function specialFilterVerification(numOfFilterChilds, wrapperOfElementList, fieldTableMap, x_cell, wrapperBodyPanel)
{
  // First element of the filter list.
  var filterElement = Sys.Browser("*").Page("https://replen*.demandlink.com/?UserName=*").TextNode(0);
  var filterElementSelected;
  // Remove the elements on the top that are not TextNode(integer).
  var realNumberOfChilds = numOfFilterChilds - 3;
  var randomElementIndex = 0;
  var maxIndexingValue = 200;
  
  if(filterElement.contentText == "The results could not be loaded.")
  {
    // Nothing to verify, there are no elements in the list to be displayed.
    Log.Warning("There is no element in the list to be loaded.");
    return;
  }
  else
  {
  
    // Limit the indexing if it is too big to reduce the time of searching.
    if(realNumberOfChilds > maxIndexingValue)
    {
      realNumberOfChilds = maxIndexingValue;
    }
    // Generate randomly the index of the child elements according to the number existing.
    randomElementIndex = Math.floor((Math.random() * 1000)) % realNumberOfChilds;
    // Get the random object.
    // Elements of the list are always in this path, except for the wrapper of the elements that changes the
    // name, instead of an index, that why it is as input of this function.
    //wrapperOfElementList.Refresh();
    filterElementSelected = Sys.Browser("*").Page("https://replen*.demandlink.com/?UserName=*").TextNode(randomElementIndex);
    var valueOfFilterApplied = filterElementSelected.contentText;
    // If there are more elements in the list that could be displayed.
    var heightValueOfElement;
    var visibleTopWindowsList = wrapperOfElementList.ScreenTop;
    var visibleBottomWindows = visibleTopWindowsList + wrapperOfElementList.Height;
    var iterVar = 0;
    if(wrapperOfElementList.scrollHeight !=  wrapperOfElementList.Height)
    {
      while(iterVar < realNumberOfChilds)
      {
        filterElement = Sys.Browser("*").Page("https://replen*.demandlink.com/?UserName=*").TextNode(iterVar);
        heightValueOfElement = filterElement.Height;
        if(filterElementSelected.ScreenTop >= visibleTopWindowsList && 
        filterElementSelected.ScreenTop <= visibleBottomWindows + 10) // Seems to be 10 less pixel difference there
        {
          filterElementSelected.Click();
          Aliases.browser.pageReplenDemandlinkCom.Wait();
          break;
        }
        else
        {
          wrapperOfElementList.scrollTop = wrapperOfElementList.scrollTop + heightValueOfElement;
          Aliases.browser.pageReplenDemandlinkCom.Wait();
        }
        iterVar = iterVar + 1;
      }
    }
    else
    {
      filterElementSelected.Click();
      Aliases.browser.pageReplenDemandlinkCom.Wait();
    }
    Log.Message("Filter Applied : " + valueOfFilterApplied);
    // Verify if the content of the filter applied it is correct in the fields.
    aqUtils.Delay(7000, "Delay for the Page.");
    fieldTableMap.Refresh();
    wrapperBodyPanel.Refresh();
    var numOfRows = fieldTableMap.RowCount - 1;
    var iterVar;
    var valueOfCurCell;
    var boxFilterSelectedValue;
    var noQuoutevalueFilterApplied;
    
    // Verify that the same selected item is equal
    // to the element in the body box below filter list.
    boxFilterSelectedValue = wrapperBodyPanel.contentText.split("\n"); // remove "new line"
    noQuoutevalueFilterApplied = valueOfFilterApplied.replace("'", ""); // Remove "'" quote if some.
    aqObject.CompareProperty(noQuoutevalueFilterApplied, cmpEqual, boxFilterSelectedValue[1], true, 2);
    
    // An Error when apply the filter.
    if(fieldTableMap.Cell(2, 0).contentText == "No rows match current filters")
    {
      Log.Warning("There is no rows for the match filter.")
      return;
    }
    
    // Verify the content of the field.
    // Cell always start on 1.
    // Verify that the filter applied is the same as the content of the column cell.
    for(iterVar = 2; iterVar <= numOfRows; iterVar++)
    {
      valueOfCurCell = fieldTableMap.Cell(iterVar, x_cell).contentText;
      aqObject.CompareProperty(valueOfFilterApplied, cmpEqual, valueOfCurCell, true, 2);
    }
  }
}