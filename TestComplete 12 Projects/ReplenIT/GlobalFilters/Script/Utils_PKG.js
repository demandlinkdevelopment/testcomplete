﻿function selectEditColumns(page, topTabsSelectoStr)
{
  // Hide all the columns and selects only the
  // one are tested in this Global Filter test:
  // and two more, this to avoid too much data in the 
  // testing.
  // Only when the current tab is "Edit".
  if(topTabsSelectoStr == "Edit")
  {
  // Select Add/Remove Column button
    page.panelMain.Click(193, 165);
    page.Wait();
    // Click "Hide All".
    page.panelRows.Click(72, 20);
    page.Wait();
    // Click on "Store ID".
    page.panelRows.Click(72, 305);
    page.Wait();
    // Click on "Store Desc".
    page.panelRows.Click(72, 340);
    page.Wait();
    // Click on "Weeks on Exception".
    page.panelRows.Click(210, 200);
    page.Wait();
    // Click on "Avail Units".
    page.panelRows.Click(510, 200);
    page.Wait();
    // Click on "On Ad".
    page.panelRows.Click(510, 410);
    page.Wait();
    // Go out of the column menu by clicking outside.
    page.panelMain.Click(207, 99);
    page.Wait();
  }
}

function selectsNumberOfRows(page, howManyRows)
{
  // Selects how many rows.
  page.panelMain.Click(75, 165);
  page.Wait();
  if(howManyRows == 10)
  {
    page.panelRows.Click(74, 18);
  }
  else if(howManyRows == 50)
  {
    page.panelRows.Click(74, 55);
  }
  else if(howManyRows == 100)
  {
    page.panelRows.Click(74, 92);
  }
  else if(howManyRows == 500)
  {
    page.panelRows.Click(74, 120);
  }
  else if(howManyRows == 1000)
  {
    page.panelRows.Click(74, 150);
  }
  else if(howManyRows == 2000)
  {
    page.panelRows.Click(74, 190);
  }
  else // 10 rows
  {
    page.panelRows.Click(74, 18);
  }
  page.Wait();
}

// Click into the Global filter selected.
// Return : the value of the status of the checkbox.
//  true : checkbox checked.
//  false: checkbox not checked.   
function selectorGlobalFilterCheckbox(selectorOption, checked, globalPanel, page)
{
  var checkboxCurrStatus;
  if(selectorOption == "Weeks On Exception" || selectorOption == "MaxWeeksOnException")
  {
    checkboxCurrStatus = globalPanel.panelException.Checkbox.checked;
    if(checkboxCurrStatus == checked)
      return checkboxCurrStatus;
    else
      globalPanel.Click(74,405);
  }
  else if(selectorOption == "Avail Units" || selectorOption == "Available")
  {
    checkboxCurrStatus = globalPanel.panelAvail.Checkbox.checked;
    if(checkboxCurrStatus == checked)
      return checkboxCurrStatus;
    else
      globalPanel.Click(74,45);
  }
  else if(selectorOption == "On Ad")
  {
    checkboxCurrStatus = globalPanel.panelOnAd.Checkbox.checked;
    if(checkboxCurrStatus == checked)
      return checkboxCurrStatus;
    else
      globalPanel.Click(74,205);
  }
  else if(selectorOption == "Seasonal")
  {
    checkboxCurrStatus = globalPanel.panelInSeason.Checkbox.checked;
    if(checkboxCurrStatus == checked)
      return checkboxCurrStatus;
    else
      globalPanel.Click(74, 245);
  }
  page.Wait();
  // Case the value desire and the current are the same.
  return !checkboxCurrStatus;
}

// This function selects the Top tab.
// Returns: the Cell panel related to the top tab.
function selectorTopTabs(topTabsSelectoStr, page)
{
  var cellsPanel, panelTopTabs, topTabObject;
  
  // Select the panel, cell tables accoring the arrange of the top tab.
  if(topTabsSelectoStr == "Edit")
    cellsPanel = page.panelEdittableWrapper.panel.panel;
  else if(topTabsSelectoStr == "Stores")
    cellsPanel = page.panelStoretableWrapper.panel;
  else if(topTabsSelectoStr == "Subregion")
    cellsPanel = page.panelSubregtableWrapper.panel;
  else if(topTabsSelectoStr == "Items")
    cellsPanel = page.panelItemtableWrapper.panel;
  else if(topTabsSelectoStr == "FarmID")
    cellsPanel = page.panelFarmtableWrapper.panel;
  else if(topTabsSelectoStr == "Exceptions")
    cellsPanel = page.panelExceptiontableWrapper.panel;
  page.Wait();
  
  // Tabs of the top of the web page.
  panelTopTabs = page.panelWrapper.panel.panel.panel;
  // Selects the top tab of the webpage.
  topTabObject = panelTopTabs.FindChild(new Array("contentText"), new Array(topTabsSelectoStr));
  page.Wait();
  if(topTabObject != null)
    topTabObject.Click();
    page.Wait();
    
  return cellsPanel;
}
// This function compares if the object is greater, less or equal depending on the string colected from the cell
// Return:
//  0 - the first element is greater than the second
//  1 - the first element is less than the second.
//  2 - both are equal.
function compareCellObjects(firstCellObjectStr, secondCellObjectStr, exception)
{
  // Verify which one is, string, number, date.
  var resultOfComparing1st = findStringPattern(firstCellObjectStr);
  var resultOfComparing2nd = findStringPattern(secondCellObjectStr);
  // Create variables to track and compare.
  var firstCellObjectInt, secondCellObjectInt;
  var firstCellDateArray, secondCellDateArray;
  var firstCellObjectDate, secondCellObjectDate;
  
  // Compare Dates.
  if((resultOfComparing1st == 4 && resultOfComparing2nd == 4) || exception == "Last Ship")
  {
    // Create a Date Object
    firstCellObjectDate = new Date();
    // If the string is empty, create a Date object to equal to 0.
    if(firstCellObjectStr == "")
    {
      firstCellObjectDate.setFullYear(0,0,0);
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    else
    {
      firstCellDateArray = splitStringBasePattern(firstCellObjectStr);
      firstCellObjectDate.setFullYear(parseInt(firstCellDateArray[2]),parseInt(firstCellDateArray[0])-1, parseInt(firstCellDateArray[1]));
      firstCellObjectDate.setHours(0); firstCellObjectDate.setMilliseconds(0); 
      firstCellObjectDate.setMinutes(0); firstCellObjectDate.setSeconds(0);
    }
    
    // Create a Date Object.
    secondCellObjectDate = new Date();
    // If the string is empty, create a Date object to equal to 0.
    if(secondCellObjectStr == "")
    {
      secondCellObjectDate.setFullYear(0,0,0);
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    else
    {
      secondCellDateArray = splitStringBasePattern(secondCellObjectStr);
      secondCellObjectDate.setFullYear(parseInt(secondCellDateArray[2]),parseInt(secondCellDateArray[0])-1, parseInt(secondCellDateArray[1]));
      secondCellObjectDate.setHours(0); secondCellObjectDate.setMilliseconds(0); 
      secondCellObjectDate.setMinutes(0); secondCellObjectDate.setSeconds(0);
    }
    // Compare the Date objects:
    if(firstCellObjectDate.getTime() > secondCellObjectDate.getTime())
      return 0; // it is greater.
    else if(firstCellObjectDate.getTime() < secondCellObjectDate.getTime())
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare Strings.
  // Exception must be considered when the string differs when there is no info.
  else if((resultOfComparing1st == 3 && resultOfComparing2nd == 3) || exception == "Farm ID" || exception == "Zone")
  {
    // No need to transform, work directly with strings.
    // Compare:
    if(firstCellObjectStr > secondCellObjectStr)
      return 0; // it is greater.
    else if(firstCellObjectStr < secondCellObjectStr)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  // Compare numbers.
  else if(resultOfComparing1st <= 2 && resultOfComparing2nd <= 2)
  {
    firstCellObjectInt = parseFloat(splitStringBasePattern(firstCellObjectStr));
    secondCellObjectInt = parseFloat(splitStringBasePattern(secondCellObjectStr));
    // Compare:
    if(firstCellObjectInt > secondCellObjectInt)
      return 0; // it is greater.
    else if(firstCellObjectInt < secondCellObjectInt)
      return 1; // it is less than.
    else
      return 2; // They are equal.
  }
  else
    Log.Error("Elements in the Cells are not the same type or has the exception.")
}

// Function to find what type of pattern is could be string, number (with or without symbol) and Date.
// Return:
//  0 : if is a number with or without commas.
//  1 : if is a number with or withot comma and the money symbol.
//  2 : if is a number with or withot comma and the percentage symbol.
//  3 : if is a string.
//  4 : if is a Date.
function findStringPattern(inputString)
{
  // Just Numbers with or without commas
  if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 0;
  // Return "$50". like (-or not -)$xx__.xx__ and with or without commas
  else if(inputString.search(/^-?\$[0-9]+(,[0-9]+)*?\.?[0-9]*?$/) != -1)
    return 1;
    // Return "%" in percentage, like (-or not -)xx__.xx__% wit or without commas
  else if(inputString.search(/^-?[0-9]+(,[0-9]+)*?\.?[0-9]*?%+$/) != -1)
    return 2;
  else if(inputString.search(/^([1-9]|1[012])[/.]([1-9]|[12][0-9]|3[01])[/.](19|20)[0-9][0-9]$/) != -1)
    return 4;
  // Could be a String, only option left.
  else
    return 3;
}
// Function in charge of split the string of a cell depending of the result
// of which pattern is.
// Return: in string in case it is a string, no spli is made into strings
//         or floating - according to javascript all numbers are represented
//         in 64-bit floating points, decimals, integers, etc.
function splitStringBasePattern(inputString)
{
  var whichPattern;
  var resultString = "";
  var result;
  // Call the pattern function to verify how to split.
  whichPattern = findStringPattern(inputString);
  
  switch(whichPattern)
  {
    case 0:
    // Just decimals, remove commas
      resultString = inputString.replace(/\,/g, "");
      result = parseFloat(resultString);
      break;
    case 1:
    // Remove "$" and commas.
      resultString = inputString.replace(/\,|\$/g, "");
      result = parseFloat(resultString);
      break;
    case 2:
    // Remove "%" and commas
      resultString = inputString.replace(/\,|\%/g, "");
      result = parseFloat(resultString);
        break;
    // Do nothing in case of a string
    case 3:
      result = inputString;
      break;
    case 4:
      result = inputString.split("/");
      break;
    // Do nothing in case of a String
    default:
      result = inputString;
  }
  return result;
}

// Only granted access to the function used outside world.
module.exports.selectEditColumns = selectEditColumns;
module.exports.selectsNumberOfRows = selectsNumberOfRows;
module.exports.compareCellObjects = compareCellObjects;
module.exports.selectorGlobalFilterCheckbox = selectorGlobalFilterCheckbox;
module.exports.splitStringBasePattern = splitStringBasePattern;
module.exports.selectorTopTabs = selectorTopTabs;