﻿// Include some functions.
var Utils_PKG = require("Utils_PKG");

function Test1()
{
  var page;
  var form;
  var textbox;
  var cellsPanel;
  // Login information variables.
  var addr_ReplenIT = "https://replen3.demandlink.com/Home/Login";
  var userName = "testreplenityoungs";
  var password = "Replenit123";
  var howManyRows = 10; // options 10, 50, 100, 500, 1000 and 2000.
  // var defaultOrAllColumns = true; // true: default columns, false: all columns.
  // Selector of the top tabs: "Edit", "Stores", "Subregion", "Items", "FarmID" and "Exceptions".
  var topTabsSelectoStr = "Edit";
  // Avail Units = Available in other top tabs.
  var listOfColumsToVerify = new Array("Weeks On Exception", "MaxWeeksOnException","Avail Units", "On Ad", "Available"); 
  
  // Variables to keep track of the values.
  var columnOffset, rowOffset;
  var numColumns, numRows, rowNumber;
  var arrayPropName = new Array("ColumnIndex", "RowIndex"), arrayPropValue;
  var topRowCellsTable;
  var listOfColumns = [], individualColumn;
  var allCellsTableEditable, cellOfTheTable, compareCellObjectResults;
  //var itemIsInTheArraySearch = false;
  var firstSampleStr, secondSampleStr, tempCellValue;
  var panelGlobalSideNav;

  // Start Test:
  // - Open Browser in the webpage
  // - Login and wait.
  Browsers.Item(btChrome).Navigate(addr_ReplenIT);
  page = Aliases.browser.pageReplenDemandlinkComHomeLogo;
  form = page.form;
  textbox = form.textboxUsername;
  textbox.SetText(userName);
  textbox.Keys("[Tab]");
  form.passwordboxPassword.SetText(password);
  textbox.Keys("[Tab]");
  form.submitbuttonLogin.Keys("[Enter]");
  page.Wait();
  // Save the left side navigation panel.
  panelGlobalSideNav = page.panelMysidenav.panelCollapseone.panel
  page.Wait();
  
  // All the cells of the panel.
  //cellsPanel = page.panelEdittableWrapper.panel.panel;
  cellsPanel = Utils_PKG.selectorTopTabs(topTabsSelectoStr, page);
    
  // Selects how many rows.
  Utils_PKG.selectsNumberOfRows(page, howManyRows);
  
  // Hide all the columns and selects only the
  // one are tested in this Global Filter test:
  // and two more, this to avoid too much data in the 
  // testing.
  // Only when the current tab is "Edit".
  Utils_PKG.selectEditColumns(page, topTabsSelectoStr);
  
  // There is no CheckBox column in this Test.
  columnOffset = 1;
  // Edit seems to have one extra row in there.
  if(topTabsSelectoStr == "Edit")
    rowOffset = 2;
  else
    rowOffset = 1;

  // Number of columns.
  numColumns = (cellsPanel.table.ChildCount / cellsPanel.table.RowCount)-columnOffset;
  // Rows of the cell table (Seems to have two or one hidden elements):
  numRows = cellsPanel.tableAllCellObjects.RowCount - rowOffset;
  
  // Go out if there is no element in the table.
  if(numRows <= 1 || numColumns <= 0)
  {
    Log.Message("No Elements to compare in the table.");
    return;
  }
  // NO NEED TO MOVE THE SCROLL.
  // Save all the columns in the array
  topRowCellsTable = cellsPanel.table;
  rowNumber = 0; // Row for the title of each column
  for(i = 0; i <= numColumns; i++)
  {
    arrayPropValue = new Array(i+(columnOffset-1), rowNumber);
    individualColumn = topRowCellsTable.FindChild(arrayPropName, arrayPropValue);
    listOfColumns.push(individualColumn);
  }
  // Edit tab: cells information starts in the 2 row.
  if(topTabsSelectoStr == "Edit")
    rowNumber = 2;
  else
    rowNumber = 1;
  // Pointer to all table of the cells with the info to be verified.
  allCellsTableEditable = cellsPanel.tableAllCellObjects;
  for(columnCounter = 0; columnCounter <= numColumns; columnCounter++)
  {
    // Retrive the Object Column of the top table List.
    //individualColumn = listOfColumns[columnCounter];
    individualColumn = listOfColumns.shift();
    // Search if the column is in the array of column to be verified,
    // if not continue to the next column in the list.
    if(listOfColumsToVerify.indexOf(individualColumn.contentText) != -1)
    {
      // Check the current column to not checked.
      Utils_PKG.selectorGlobalFilterCheckbox(individualColumn.contentText, false, panelGlobalSideNav, page);
      // Clear a track variable for the next iteration.
      tempCellValue = "";
      // First sorted:
      individualColumn.Click();
      page.Wait();
      // Look for the first cell in the column.
      arrayPropValue = new Array(columnCounter+(columnOffset-1), rowNumber);
      cellOfTheTable = allCellsTableEditable.FindChild(arrayPropName, arrayPropValue);
      page.Wait();
      // First sample of the value to identify in which status is: ascending or descending.
      // For this TEST, need to be in descending to ascending.
      firstSampleStr = cellOfTheTable.contentText;
      // Click the column to be sorted the next time and see which is the current status.
      individualColumn.Click();
      page.Wait();
      // second sample of the value to identify in which status is: ascending or descending depending on first sample.
      secondSampleStr = cellOfTheTable.contentText;
      // Request if the object is greater, less or equal than depending of the type of string
      // that could be a Date, String or number.
      // This TEST only verify numbers.
      // ******************************
      // Verify for Global filter not set - not set above.
      compareCellObjectResults = Utils_PKG.compareCellObjects(firstSampleStr, secondSampleStr, individualColumn.contentText);
      if(compareCellObjectResults == 1)// if it is in ascending to descending click it.
      {                                // this is because need to show the smallest value to see if there is more than 0.
        individualColumn.Click();
        page.Wait();
      }
      // If both values are equal. That means that when applying the filter may no have any field, if the same value are zero.
      // * NOT THAT MUCH TO VERIFY HERE.
      Log.Checkpoint("Global Filter for \"" + individualColumn.contentText + "\" is not activated and the value of the first row of the Column" 
      + individualColumn.contentText + " is equal to: " + cellOfTheTable.contentText);        
      // Save value to the next verifications if the values are equal for descending and ascending.
      if(compareCellObjectResults == 2)
      {
        //Log.Checkpoint("All the element in the column \""+ individualColumn.contentText + "\" are equal.");
        if(cellOfTheTable.contentText == "0" && 
        (individualColumn == "Weeks On Exception" || individualColumn.contentText == "Avail Units" || individualColumn.contentText == "Available" 
        || individualColumn.contentText == "MaxWeeksOnException"))
          Log.Checkpoint("And the value of it is 0 (zero) and the availability will display 0 when is no info.");
        if(cellOfTheTable.contentText == "No" && individualColumn.contentText == "On Ad")
          Log.Checkpoint("And the value of it is No and the availability will display No when is no info.");
          tempCellValue = cellOfTheTable.contentText;
      }
      // ******************************
      // Verify for Global filter is set.
      // ******************************
      // Activate the Global Filter.
      Utils_PKG.selectorGlobalFilterCheckbox(individualColumn.contentText, true, panelGlobalSideNav, page);
      
      // If both values are equal. That means that when applying the filter may no have any field, if the same value are zero.
      // Check the number of rows, if the are 3 or less rows and the previous comparison was equal and that equality is when 
      // cells has the value of none data due the global filter, it is OK to have it.
      // This verify that there is no element due the previous comparison when equal to the empty value make the cells to be 
      // only with one general row with the legend of "No row match current filter.".
      if(compareCellObjectResults == 2 && cellsPanel.tableAllCellObjects.RowCount <= 3 && (tempCellValue == "0" || tempCellValue == "No"))
      {
        Log.Checkpoint("No columns due the Global filter \"" + individualColumn.contentText 
        + "\" applied, meaning that the is no data to be displayed and the filter was applied correctly.");
      }
      //This verify:
      // 1: the vaules are the same before applied the Global filter, but there is a least one element.
      // 2: the values are different before applied the Global filter
      else if(cellsPanel.tableAllCellObjects.RowCount >= 3)// There is data in the cell panel, filter is not displayed zero or no.
      {
        if(individualColumn.contentText == "Weeks On Exception" || individualColumn.contentText == "Avail Units"
          || individualColumn.contentText == "Available" || individualColumn.contentText == "MaxWeeksOnException") // numbers in the cell
        {
          if(Utils_PKG.splitStringBasePattern(cellOfTheTable.contentText) > 0)
            Log.Checkpoint("Data when the Global Filter \"" + individualColumn.contentText + "\" is applied is greater than 0."
            + "Global filter was applied correctly");
          else
            Log.Error("Global filter \"" + individualColumn.contentText + "\" was not applied correctly");
        }
        else if(individualColumn.contentText == "On Ad")// String in the cell.
        {
          if(cellOfTheTable.contentText == "Yes")
            Log.Checkpoint("Data when the Global Filter \"" + individualColumn.contentText + "\" is applied is equal to Yes."
            + "Global filter was applied correctly");
          else
            Log.Error("Global filter \"" + individualColumn.contentText + "\" was not applied correctly");
        }
      }
      else
      {
        Log.Error("Global filter \"" + individualColumn.contentText + "\" was not applied correctly");
      }
      // De-activate the Global Filter.
      Utils_PKG.selectorGlobalFilterCheckbox(individualColumn.contentText, false, panelGlobalSideNav, page);
    }
    else
    {
      continue;
    }
  }
  
  // Go back to default of displaying 10 rows.
  page.panelMain.Click(65, 142);
  page.Wait();
  page.panelRows.Click(74, 18);
  page.Wait();
  // Go to default columns once test finishs.
  // Only when the current tab is "Edit".
  if(topTabsSelectoStr == "Edit")
  {
    page.panelMain.Click(193, 148);
    page.Wait();
    page.panelRows.Click(72, 76);
    page.Wait();
    page.panelMain.Click(207, 94);
    page.Wait();
  }
  // End of Test.
  Log.Message("Test of Global Filters completed.")
  return;
}